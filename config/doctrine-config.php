<?php
namespace VStancescu\Users;

return [
    // Doctrine connection config
    'settings.connection' => [
        'driver'   => 'pdo_mysql',
        'host'     => \DI\get('settings.storage.host'),
        'user'     => \DI\get('settings.storage.user'),
        'password' => \DI\get('settings.storage.password'),
        'dbname'   => \DI\get('settings.storage.databaseName'),
        'charset'   => 'utf8',
    ],
];
