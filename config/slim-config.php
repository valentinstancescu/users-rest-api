<?php
namespace VStancescu\Users;

use VStancescu\Users\Slim\Handlers\Error;
use VStancescu\Users\Slim\Handlers\NotAllowed;
use VStancescu\Users\Slim\Handlers\NotFound;
use VStancescu\Users\Slim\Handlers\PhpError;

return [
    'settings.displayErrorDetails' => \DI\get('settings.developmentMode'),
    'notFoundHandler' => \DI\object(NotFound::class),
    'notAllowedHandler' => \DI\object(NotAllowed::class),
    'errorHandler' => \DI\object(Error::class),
    'phpErrorHandler' => \DI\object(PhpError::class),
];
