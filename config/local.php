<?php
namespace VStancescu\Users;

return [
    'settings.developmentMode' => true, // in production set to false

    // storage config
    'settings.storage.host' => getenv('MYSQL_HOST'),
    'settings.storage.user' => getenv('MYSQL_USER') ?: 'devel',
    'settings.storage.password' => getenv('MYSQL_PASSWORD') ?: 'test123',
    'settings.storage.databaseName' => getenv('MYSQL_DBNAME') ?: 'meetup',

    // Used for Authentication
    'settings.ApiKey' => 'DEFAULT_API_KEY',
];
