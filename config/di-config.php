<?php

namespace VStancescu\Users;

use Doctrine\Common\Persistence\ObjectManager;
use VStancescu\Users\Common\Api\ResponseBuilder\ErrorApiResponseBuilderInterface;
use VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder;
use VStancescu\Users\Doctrine\ObjectManagerFactory;
use VStancescu\Users\Entity\Group;
use VStancescu\Users\Entity\User;
use VStancescu\Users\Entity\UsersGroups;
use VStancescu\Users\Group\Api\ResponseBuilder\GroupApiJsonResponseBuilder;
use VStancescu\Users\Group\Api\ResponseBuilder\GroupApiResponseBuilderInterface;
use VStancescu\Users\Repository\GroupRepository;
use VStancescu\Users\Repository\UserRepository;
use VStancescu\Users\Repository\UsersGroupsRepository;
use VStancescu\Users\User\Api\ResponseBuilder\UserApiJsonResponseBuilder;
use VStancescu\Users\User\Api\ResponseBuilder\UserApiResponseBuilderInterface;

return [
    ErrorApiResponseBuilderInterface::class => \DI\object(JsonErrorApiResponseBuilder::class),
    UserApiResponseBuilderInterface::class => \DI\get(UserApiJsonResponseBuilder::class),
    GroupApiResponseBuilderInterface::class => \DI\get(GroupApiJsonResponseBuilder::class),

    ObjectManager::class => \DI\factory([ObjectManagerFactory::class, 'createObjectManager']),

    UserRepository::class => function (ObjectManager $objectManager) {
        return $objectManager->getRepository(User::class);
    },

    GroupRepository::class => function (ObjectManager $objectManager) {
        return $objectManager->getRepository(Group::class);
    },

    UsersGroupsRepository::class => function (ObjectManager $objectManager) {
        return $objectManager->getRepository(UsersGroups::class);
    },
];
