<?php

namespace VStancescu\Users;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use VStancescu\Users\Slim\Application;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$application = new Application();
$container = $application->getContainer();
$entityManager = $container->get(ObjectManager::class);

return ConsoleRunner::createHelperSet($entityManager);
