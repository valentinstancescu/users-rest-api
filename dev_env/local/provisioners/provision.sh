#!/usr/bin/env bash

mkdir /home/ubuntu/.phpstorm_helpers
chown ubuntu:ubuntu /home/ubuntu/.phpstorm_helpers

# this symbolic link makes possible to use "/usr/bin/php" as interpreter path in IDE settings
ln --force --symbolic /var/www/users/dev_env/local/docker/tools/run-php-in-users-php.sh /usr/bin/php

# this symbolic link makes possible to use "/usr/bin/composer" as composer binary
ln --force --symbolic /var/www/users/dev_env/local/docker/tools/composer.sh /usr/bin/composer

# make XDebug on by default
echo -e "\nexport XDEBUG_STATUS=1" >> /etc/profile
echo -e "\nexport XDEBUG_STATUS=1" >> /etc/bash.bashrc

apt-get install -y realpath
apt-get autoremove -y

# enable cli prompt color
sed -i 's/#force/force/' .bashrc
