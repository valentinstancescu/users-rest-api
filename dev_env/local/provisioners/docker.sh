#!/usr/bin/env bash

# This script is going to run at each vagrant up. It's going to be ran as the vagrant user.
#
# It does the following:
#    1. Build images, builds them always for the moment,
#       because if the images already exist it is very fast to rebuild them anyway.
#    2. Create containers if they do not exist or start if they exist and are not running
#       If services containers exist:
#       - start service containers if are not already running
#       If services containers do not exist:
#       - run service containers (create and start)

DOCKER_DIR=/var/www/users/dev_env/local/docker

checkDockerContainer() {
    if [ -z "$1" ]; then
        printf "Error: Missing checkDockerContainer param."
        exit
    fi

    # return code 0 - container exists, start it if it is not already running
    # return code 1 - container does not exist - create and start it
    if docker container inspect $1 1>/dev/null 2>&1; then
        STATE=$(docker container inspect --format {{.State.Running}} $1 2>/dev/null)
        if [ "$STATE" == "true" ]; then
            printf "==== $1 is already running.\n"
            return
        fi

        printf "==== $1 is stopped. Starting it now.\n"
        docker start $1
    else
        printf "==== $1 does not exist. Creating it now.\n"
        ${DOCKER_DIR}/run/run-${1}.sh

         # if mysql container is built run database setup also
        if echo $1 | grep --quiet '\-mysql'; then
            printf "  == Running setup for mysql.\n"
            sleep 2
            ${DOCKER_DIR}/run/run-users-mysql-setup.sh
        fi
    fi
}


printf "\n==== Images ====\n"

${DOCKER_DIR}/build/build-all.sh

printf "\n==== Networks ====\n"

${DOCKER_DIR}/run/start-network.sh

printf "\n==== Containers ====\n"

source ${DOCKER_DIR}/run/containers.sh
for CONTAINER in "${CONTAINERS[@]}"; do
    checkDockerContainer $CONTAINER
done
