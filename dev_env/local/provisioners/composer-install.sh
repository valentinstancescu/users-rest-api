#!/usr/bin/env bash

cd /var/www/users
cp config/local.php.dist config/local.php
composer install
