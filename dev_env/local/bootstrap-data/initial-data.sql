insert  into `groups`(`id`,`name`,`description`,`city`,`country`,`date_created`) values (1,'Group Name1','Short description of the group','Bucharest','RO','2018-08-06 13:39:10'),(2,'Group Name2','Short description of the group','Bucharest','RO','2018-08-06 13:49:10'),(3,'Group Name3','Short description of the group','Bucharest','RO','2018-08-06 13:59:10');

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`description`,`city`,`country`,`date_created`,`user_status`) values (1,'Valentin','Stancescu','sv1@mailinator.com','Short user description','Bucharest','RO','2018-08-06 10:35:53','ACTIVE'),(2,'Valentin','Popa','sv2@mailinator.com','Short user description','Bucharest','RO','2018-08-06 10:35:57','ACTIVE'),(3,'Marco','van Basten','sv3@mailinator.com','Short user description','Amsterdam','NL','2018-08-06 10:36:14','ACTIVE'),(4,'Ruud','Gullit','sv4@mailinator.com','Short user description','Rotterdam','NL','2018-08-06 10:36:34','ACTIVE'),(5,'Timo','Baks','sv5@mailinator.com','Short user description','Utrecht','NL','2018-08-06 11:13:07','ACTIVE'),(6,'Engin','Lofca','sv6@mailinator.com','Short user description','Istanbul','TR','2018-08-06 11:13:21','ACTIVE'),(7,'Emre','Guzer','sv7@mailinator.com','Short user description','Ankara','TR','2018-08-06 11:13:29','ACTIVE');

insert  into `users_groups`(`id`,`user_id`,`group_id`,`is_admin`,`date_created`) values (1,1,1,1,'2018-08-06 13:39:22'),(2,3,1,0,'2018-08-06 13:39:35'),(3,2,1,0,'2018-08-06 13:39:48'),(4,5,2,1,'2018-08-06 14:16:37'),(5,6,3,1,'2018-08-06 14:16:49'),(6,1,2,0,'2018-08-06 14:19:05');

insert  into `friends`(`user_id`,`friend_user_id`) values (1,2),(1,4),(1,5),(1,6),(1,7),(2,1),(4,1),(5,1),(6,1),(7,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
