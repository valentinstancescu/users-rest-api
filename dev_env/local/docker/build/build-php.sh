#!/usr/bin/env bash

script_dir=$(realpath $(dirname "$0"))

cd ${script_dir}

buildTag=${1:-"latest"}

docker build --build-arg fromTag=${buildTag} -t users-php:${buildTag} ${script_dir}/php
