#!/usr/bin/env bash

cd $(realpath $(dirname "$0"))

./build-base.sh

./build-php.sh
./build-apache-php.sh

./build-mysql.sh

docker rmi $(docker images -q --filter "dangling=true") 2> /dev/null
