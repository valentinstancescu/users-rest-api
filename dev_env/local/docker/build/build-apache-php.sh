#!/usr/bin/env bash

script_dir=$(realpath $(dirname "$0"))

cd ${script_dir}

buildTag=${1:-"latest"}

docker build --build-arg fromTag=${buildTag} -t users-apache-php:${buildTag} ${script_dir}/apache-php
