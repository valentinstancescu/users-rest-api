#!/usr/bin/env bash

script_dir=$(realpath $(dirname "$0"))

cd ${script_dir}

buildTag=${1:-"latest"}

docker build -t users-base:${buildTag} ${script_dir}/base
