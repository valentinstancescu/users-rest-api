#!/usr/bin/env bash

docker exec users-mysql \
    sh -c "echo \"DELETE FROM mysql.user where User = ''; CREATE DATABASE meetup; CREATE USER 'devel' IDENTIFIED BY 'test123'; GRANT ALL PRIVILEGES ON meetup.* TO 'devel'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;\" | mysql -u root -h localhost"

docker exec users-apache-php \
    sh -c "vendor/bin/doctrine orm:schema-tool:create"

docker exec users-apache-php \
    sh -c "vendor/bin/doctrine dbal:import dev_env/local/bootstrap-data/initial-data.sql"
