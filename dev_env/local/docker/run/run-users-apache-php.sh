#!/usr/bin/env bash

# host paths
PROJECT_DIR=$(realpath $(dirname "$0")/../../../..)
RUN_DIR=$PROJECT_DIR/dev_env/local/docker/run/users-apache-php
LOGS_DIR=/var/www/logs/users
PHPSTORM_HELPERS_DIR=/home/ubuntu/.phpstorm_helpers

# container paths
WORK_DIR=/var/www/users

docker run -d \
    -e "XDEBUG_STATUS=$XDEBUG_STATUS" \
    --env-file "$RUN_DIR/settings.env" \
    --name users-apache-php \
    --net users-network \
    -p 192.168.50.10:80:80 \
    -v $RUN_DIR/vhost.conf:/etc/httpd/conf.d/vhost.conf \
    -v $RUN_DIR/99-custom-php.ini:/etc/php.d/99-custom-php.ini \
    -v $PROJECT_DIR:$WORK_DIR \
    -v $PHPSTORM_HELPERS_DIR:$PHPSTORM_HELPERS_DIR \
    -w $WORK_DIR \
    users-apache-php
