#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")"

dashes="================================"

printf "\n%s\n Deleting containers\n%s\n" ${dashes} ${dashes}
docker rm -fv $(docker ps -qa) 2> /dev/null

printf "\n%s\n Recreating networks \n%s\n" ${dashes} ${dashes}
./remove-network.sh
./start-network.sh

./run-users-apache-php.sh
./run-users-mysql.sh

sleep 2

./run-users-mysql-setup.sh
