#!/usr/bin/env bash

docker run -d \
    --name users-mysql \
    --net users-network \
    -p 192.168.50.10:3306:3306 \
    users-mysql
