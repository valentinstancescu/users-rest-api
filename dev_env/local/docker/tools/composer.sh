#!/usr/bin/env bash

docker run \
    --interactive \
    --rm \
    --volume $(pwd):/app \
    --volume /home/ubuntu/.composer:/tmp \
    composer:1 --no-interaction "$@"
