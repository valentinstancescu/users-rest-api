<?php

use VStancescu\Users\DI\ContainerHelper;
use VStancescu\Users\Smarty\InterfaceController;

require_once dirname(__DIR__) . '/vendor/autoload.php';



/** @var VStancescu\Users\Smarty\InterfaceController $interfaceController */
$interfaceController = ContainerHelper::get(InterfaceController::class);
$interfaceController->display();
