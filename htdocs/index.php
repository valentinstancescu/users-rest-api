<?php

namespace PayU\IRetail;

use VStancescu\Users\Slim\ApplicationBuilder;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$appBuilder = new ApplicationBuilder();
$app = $appBuilder->createApplication();

$app->run();
