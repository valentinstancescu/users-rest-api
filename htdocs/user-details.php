<?php

use VStancescu\Users\DI\ContainerHelper;
use VStancescu\Users\Smarty\UserDetailsController;

require_once dirname(__DIR__) . '/vendor/autoload.php';

/** @var UserDetailsController $userDetailsController */
$userDetailsController = ContainerHelper::get(UserDetailsController::class);
$userDetailsController->display($_GET);
