# Rest API implemementation example

The application is a REST service that facilitates
social networking functionality:
- request to create a relationship with a fellow user in a group
- list of a user’s current connections (with different filtering possibilities)

 The application is designed as a microservice to be used by other applications or services.
 
 ###Communication with other entities
 
 The applications is able to receive user and group data and respond to specific queries via a couple of RESTful web services which can be consumed be other application application. 

###Main use cases and work flows

    Create new user 
        A HTTP POST request is made to the User API of the application.
        A new user entity is created and stored in the database.
        
        POST to: http://users.vstancescu.local/api/v1/user
        Sample request body:
            {
              "first-name": "SampleFristName",
              "last-name": "SampleLastName",
              "email": "sv@mailinator.com",
              "description": "Short user description",
              "city": "Bucharest",
              "country": "RO"
            }
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY
 
    Fetch user
        A HTTP GET request is made to the User API of the application.
        The user entity is read from the database and returned.
        Different filtering options are presented here.
        
        GET to: http://users.vstancescu.local/api/v1/user/{USER-ID}?show-friends=true&country=RO&same-group=true
        Sample request body:
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY
        
    Add friend to a user
        A HTTP POST resquest is made to the User API of the application.
        Two user entities are connected to eachother.
        
        POST to: http://users.vstancescu.local/api/v1/user/{USER-ID}/friend/{FRIEND-USER-ID}
        Sample request body:
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY
            
    Create new group 
        A HTTP POST request is made to the Group API of the application.
        A new group entity is created and stored in the database.
        Before calling this API a new User should be defined and then used 
        here as a founder of teh group.
        
        POST to: http://users.vstancescu.local/api/v1/group
        Sample request body:
            {
              "name": "Group Name",
              "description": "Short description of the group",
              "city": "Bucharest",
              "country": "RO",
              "foundedBy": "1"
            }
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY    
    Fetch a group
        A HTTP GET request is made to the Group API of the application.
        The group entity is read from the database and returned.
        
        GET to: http://users.vstancescu.local/api/v1/group/{GROUP-ID}
        Sample request body:
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY
            
    Add user to a group
        A HTTP POST request is made to Group API of the application.
        An existing user entity is connected to a group entity.
        
        POST to: http://users.vstancescu.local/api/v1/group/{GROUP-ID}/user/{USER-ID}
        Sample request body:
        Request headers:
            Content-Type:application/json
            Accept:application/json
            Authorization:ApiKey DEFAULT_API_KEY


##Application architecture
###Components overview

The application is composed of RESTful web services and front end interface.

**Web services:**

    User
        Exposes the functionality of creating fetching by various search filters and connecting a User entity.
    Group
        Exposes the functionality of creating a new Group entity and fetching a Group entity.


**Front end interface:**

    Displays list of users and user details


**Other components** of the application are:

    
    Tests (unit tests, located in the tests directory)
    Runtime configuration (config directory)
    Development environment scripts (dev_env directory)


**External libraries**

The application depends also on a couple of external libraries which are managed with composer.

The list of required packages can be found in the composer.json file and the source code of the libraries is located in the vendor directory.

The most important libraries are:

    Slim v3 - used for API routing and HTTP request/response handling
    Doctrine v2 - used as ORM and database abstraction layer 
    PHP-DI - used as dependency injection container
    PHPUnit - used for unit tests
    
    
###Web services components architecture

The implementations of the web services follow the same pattern, being roughly split into the same components:

####Entities

Are representations of business entities: User, Group, UsersGroups.

Between certain entities there exist association relationships which are further reflected in the Doctrine ORM mapping xml files.

####API Layer

The routing and HTTP request/response handling is provided by the Slim Framework.

    The API routes are defined in the Slim\ApplicationBuilder class.
    There is a .htaccess file located in the publicly accessible directory htdocs which rewrites the URLs of the incoming requests.
    All requests to non-existent files or directories are redirected to the htdocs/index.php file which is the starting point of the Slim Application.

A Slim middleware is used to provide a security layer for all the APIs:

    This is implemented in the Slim\Authentication package.
    All incoming HTTP requests must contain the Authorization header with a valid value.
    The required value of the header is defined in the runtime configuration, which comes either as an environment variable or is defined in the config/local.php file.
    Example of HTTP header: Authorization = ApiKey defaultApiKey.

Some application errors are also handled by the Slim Framework. There are a couple of Slim Error Handlers implemented in the Slim\Handlers package:

    404 Not Found Handler, when there is no matching route for the HTTP request URI
    405 Not Allowed Handler, when there is a matching route for the HTTP request URI but NOT the HTTP request method
    500 PHP Error Handler, when there is a php runtime error
    500 Generic Error Handler, when there are uncaught exceptions and all other php errors

Requests:

    The content type of the HTTP requests is application/json.
    They must contain the Authorization header for security purposes, as explained above.

Responses:

    HTTP response codes are set according to the logic of the processing outcome: 200 for successful responses, 400 for client errors, 500 for server errors.
    The content type of the HTTP responses is application/json.
    The success responses contain the json representations of the entities.
    The error responses have a unitary format for all the APIs.
        This is defined in the common JsonErrorApiResponseBuilder class located in the Common\Api\ResponseBuilder package.
        Basically, an error response contains the error code and error message.
        There is also the ErrorApiResponseBuilderInterface which is an abstraction meant to allow the application to be agnostic of the content type of the Response.
    All the API error response codes and messages are aggregated in the ApiResponseErrors class located in the Common\Api package.
    
    
####Service Layer

The service layer is responsible with handling the business logic and doing actions on the entities. It is responsible with creating and updating the entities.

Another purpose of the Service Layer is to hide any knowledge about an existing Storage to the upper layers of the application.

The Service usually receives a value object from a parsed request.

It returns an entity or a collection of entities on success, and an exception on error.

###Database

The application is using a relational database (MySQL). The abstraction layer is provided by the Doctrine library

##Development Environment

###Description

The project is located in the following git repository: https://bitbucket.org/valentinstancescu/users-rest-api

The development environment consists of an Ubuntu virtual machine running docker containers.

The virtual machine is provisioned and managed using Vagrant.

The vagrant provisioner and docker scripts are located in the dev_env directory.

The following docker containers are created and executed:

    users-apache-php, which is a container running an Apache web server and php
    users-mysql, which is a container running a MySQL database server
    
### Installation

In order to be able to install the Application on a local development machine, the following packages are required:

    git
    Vagrant
    Virtual Box

####Installation steps:

1. Clone the project from the git repository: `git clone https://valentinstancescu@bitbucket.org/valentinstancescu/users-rest-api.git`
2. Go to the project's root directory (where the Vagrantfile is located): `cd users-rest-api`
3. Start the Vagrant machine: `vagrant up`
4. Make the Doctrine proxies directory writable.
    Run `sudo chmod a+w src/Doctrine/ProxyEntities` from the project root, on the host machine.
    This step is done because the Doctrine proxies directory must be writable.
    This step cannot be done from the guest machine or by a Vagrant provisioner because Vagrant cannot change file permission in a "synced_folder".
5. Create `templates_c` folder for compiled templates of the front end and make it writable.
   Run `sudo chmod a+w src/templates_c` from the project root, on the host machine.
   This step is done because the templates_c directory must be writable.
   
You can log into the Vagrant machine by executing vagrant ssh.

If you want to recreate all the docker containers from scratch, there is a useful bash script: dev_env/local/docker/run/run-all.sh. You need to execute it from inside the Vagrant machine.

####Frontend access:
You can access front end interface from: `http://users.vstancescu.local/interface.php`

####API access:
You can access the API by following the routes and using the sample request provided above.

##Tests and Automation testing environment

The tests written for this project are located in the tests directory.

They are written in php using phpunit 6. The phpunit.xml configuration file is located in the root of the project.

Run tests steps:
1. Execute `vagrant ssh`
2. Go to `cd /usr/bin/`
3. Run Command `/var/www/users/vendor/phpunit/phpunit/phpunit --configuration /var/www/users/phpunit.xml`
