<?php

namespace VStancescu\Users\Common\Api\ResponseBuilder;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * @covers \VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder
 */
class JsonErrorApiResponseBuilderTest extends TestCase
{
    /** @var JsonErrorApiResponseBuilder */
    private $sut;

    protected function setUp()
    {
        $this->sut = new JsonErrorApiResponseBuilder();
    }

    public function providerValuesForErrorResponse()
    {
        return [
            'httpStatus 400' => [400, 'Some error message', '1234', '1234'],
            'httpStatus 500' => [500, 'Some error message', '1234', '1234'],
            'errorMessage empty' => [400, '', '1234', '1234'],
            'errorCode integer value is converted to string' => [400, 'Some error message', 1234, '1234'],
            'errorCode numeric string stays string' => [400, 'Some error message', '1234', '1234'],
            'errorCode literal string stays string' => [400, 'Some error message', 'random', 'random'],
        ];
    }

    /**
     * @dataProvider providerValuesForErrorResponse
     */
    public function testBuildErrorResponse($httpStatus, $errorMessage, $errorCode, $convertedErrorCode)
    {
        $jsonResponse = $this->sut->errorResponse($httpStatus, $errorCode, $errorMessage);

        $this->assertInstanceOf(ResponseInterface::class, $jsonResponse, 'Not correct object instance');
        $this->assertSame($httpStatus, $jsonResponse->getStatusCode(), 'Http status code is different');
        $this->assertSame(
            [
                'Content-Type' => ['application/json;charset=utf-8'],
            ],
            $jsonResponse->getHeaders(),
            "Response headers do not contain Content-Type json"
        );

        $responseBody = (string)$jsonResponse->getBody();
        $parsedResponseBody = json_decode($responseBody, true);
        $this->assertSame(
            [
                'error' => [
                    'message' => $errorMessage,
                    'code' => $convertedErrorCode,
                ]
            ],
            $parsedResponseBody,
            "Json response body for error does not match."
        );
    }
}
