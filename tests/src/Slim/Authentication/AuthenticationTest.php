<?php

namespace VStancescu\Users\Slim\Authentication;

use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder;
use VStancescu\Users\Common\Config;

/**
 * @covers \VStancescu\Users\Slim\Authentication\Authentication
 */
class AuthenticationTest extends TestCase
{
    /** @var Authentication */
    private $sut;

    /** @var Config | \PHPUnit_Framework_MockObject_MockObject */
    private $configMock;

    /** @var JsonErrorApiResponseBuilder | \PHPUnit_Framework_MockObject_MockObject */
    private $jsonErrorApiResponseBuilderMock;

    private $authenticationHeaderName;

    public function setUp()
    {
        $this->authenticationHeaderName = "HTTP_" . Authentication::AUTHENTICATION_HEADER_NAME;
        $this->configMock = $this->createMock(Config::class);
        $this->jsonErrorApiResponseBuilderMock = $this->createMock(JsonErrorApiResponseBuilder::class);

        $this->sut = new Authentication($this->configMock, $this->jsonErrorApiResponseBuilderMock);
    }

    public function testFailAuthorizationWhenHeaderIsMissing()
    {
        $environment = Environment::mock();
        $request = Request::createFromEnvironment($environment);
        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->jsonErrorApiResponseAssertion();

        $this->sut->__invoke(
            $request,
            $response,
            $next
        );
    }

    public function testFailAuthorizationWhenApiKeyIsWrong()
    {
        $environment = Environment::mock([
            $this->authenticationHeaderName => "ApiKey defaultApiKey",
        ]);

        $request = Request::createFromEnvironment($environment);
        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->configMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo(Authentication::AUTHENTICATION_CONFIG_KEY_NAME))
            ->willReturn(rand(1000, 9999));

        $this->jsonErrorApiResponseAssertion();

        $this->sut->__invoke(
            $request,
            $response,
            $next
        );
    }

    public function testSuccessAuthorization()
    {
        $apiKey = 'defaultApiKey';
        $environment = Environment::mock([
            $this->authenticationHeaderName => "ApiKey {$apiKey}",
        ]);

        $request = Request::createFromEnvironment($environment);
        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->configMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo(Authentication::AUTHENTICATION_CONFIG_KEY_NAME))
            ->willReturn($apiKey);

        $this->jsonErrorApiResponseBuilderMock->expects($this->never())->method('errorResponse');

        $this->sut->__invoke(
            $request,
            $response,
            $next
        );
    }

    private function jsonErrorApiResponseAssertion()
    {
        $this->jsonErrorApiResponseBuilderMock->expects($this->once())
            ->method('errorResponse')
            ->with(
                $this->equalTo(401),
                $this->equalTo(ApiResponseErrors::AUTHENTICATION_GENERIC_ERROR),
                $this->equalTo(ApiResponseErrors::ERRORS[ApiResponseErrors::AUTHENTICATION_GENERIC_ERROR])
            );
    }

}
