<?php

namespace VStancescu\Users\User\Api;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\DI\ContainerHelper;
use VStancescu\Users\Entity\User;
use VStancescu\Users\User\Service\UserService;
use VStancescu\Users\User\Service\UserServiceException;
use VStancescu\Users\User\Service\UsersToConnect;
use VStancescu\Users\User\Service\UserToCreate;


/**
 * Class UserControllerComponentTest
 * @covers \VStancescu\Users\User\Api\UserController
 * @covers \VStancescu\Users\User\Api\RequestParser\UserApiRequestParser
 * @covers \VStancescu\Users\User\Api\ResponseBuilder\UserApiJsonResponseBuilder
 */
class UserControllerComponentTest extends TestCase
{
    const VALID_FIRST_NAME = 'Valentin';
    const VALID_LAST_NAME = 'Stancescu';
    const VALID_EMAIL = 'sv1@mailinator.com';
    const VALID_DESCRIPTION = 'Short user description';
    const VALID_CITY = 'Bucharest';
    const VALID_COUNTRY = 'RO';

    /** @var UserController */
    private $userController;

    /** @var UserService | PHPUnit_Framework_MockObject_MockObject */
    private $userServiceMock;

    protected function setUp()
    {
        $this->userServiceMock = $this->createMock(UserService::class);

        $this->userController = ContainerHelper::make(UserController::class, [
            'userService' => $this->userServiceMock,
        ]);
    }

    /**
     * Tests method to create new user
     */
    public function testCreateUserSuccess()
    {
        $parsedRequestBody = $this->getCreateRequest();
        $requestStub = $this->getRequestStub('getBody', $parsedRequestBody);

        $expectedUserToCreate = new UserToCreate(
            self::VALID_FIRST_NAME,
            self::VALID_LAST_NAME,
            self::VALID_EMAIL,
            self::VALID_DESCRIPTION,
            self::VALID_CITY,
            self::VALID_COUNTRY
        );

        $this->userServiceMock->expects($this->once())->method('createUser')
            ->with($expectedUserToCreate)
            ->willReturn($this->getDefaultUserEntity());
        
        $response = $this->userController->create($requestStub);

        $this->assertSuccessResponse($response);
    }

    /**
     * Test that handles request parsing exception for missing parameters sent into request
     */
    public function testFailCreateUserForParsingException()
    {
        $parsedRequestBody = $this->getCreateRequest();
        unset($parsedRequestBody['email']);
        $requestStub = $this->getRequestStub('getBody', $parsedRequestBody);

        $response = $this->userController->create($requestStub);

        $this->assertErrorResponse(ApiResponseErrors::USER_MISSING_EMAIL, $response);
    }

    /**
     * Test that handles request user service exception
     */
    public function testFailCreateUserForServiceException()
    {
        $parsedRequestBody = $this->getCreateRequest();
        $requestStub = $this->getRequestStub('getBody', $parsedRequestBody);

        $this->userServiceMock
            ->expects($this->once())
            ->method('createUser')
            ->willThrowException(new UserServiceException(
                UserServiceException::ERRORS[UserServiceException::DUPLICATE_USER_ERROR],
                UserServiceException::DUPLICATE_USER_ERROR
            ));

        $response = $this->userController->create($requestStub);

        $this->assertErrorResponse(UserServiceException::DUPLICATE_USER_ERROR, $response);
    }

    /**
     * Tests method that adds a friend to a user with success
     */
    public function testAddFriendToUserSuccess()
    {
        $userId = 1;
        $connectedUserId = 2;

        $expectedUserToConnect = new UsersToConnect($userId, $connectedUserId);
        $this->userServiceMock->expects($this->once())->method('connectUsers')
            ->with($expectedUserToConnect)
            ->willReturn($this->getDefaultUserEntity());

        $response = $this->userController->addFriendToUser($userId, $connectedUserId);

        $this->assertSuccessResponse($response);
    }

    /**
     * Test that handles request parsing exception for bad parameters sent into request
     */
    public function testAddFriendToUserForParsingException()
    {
        $userId = 1;
        $connectedUserId = 'asd';

        $response = $this->userController->addFriendToUser($userId, $connectedUserId);

        $this->assertErrorResponse(ApiResponseErrors::USER_ID_NOT_NUMERIC, $response);
    }

    /**
     * @return string
     */
    private function getCreateRequest()
    {
        return [
            'first-name' => 'Valentin',
            'last-name' => 'Stancescu',
            'email' => 'sv1@mailinator.com',
            'description' => 'Short user description',
            'city' => 'Bucharest',
            'country' => 'RO',
        ];
    }


    /**
     * @param string $methodName
     * @param array|string|null $parsedRequestBody
     * @return \PHPUnit_Framework_MockObject_MockObject | ServerRequestInterface
     */
    private function getRequestStub(string $methodName, $parsedRequestBody)
    {
        $requestStub = $this->createMock(ServerRequestInterface::class);
        $requestStub->method($methodName)->willReturn(json_encode($parsedRequestBody));
        return $requestStub;
    }

    private function getDefaultUserEntity()
    {
        $expectedUser = $this->getMockBuilder(User::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([
                self::VALID_FIRST_NAME,
                self::VALID_LAST_NAME,
                self::VALID_EMAIL,
                self::VALID_DESCRIPTION,
                self::VALID_CITY,
                self::VALID_COUNTRY
            ])
            ->setMethods(['getId'])
            ->getMock();

        $expectedUser->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        return $expectedUser;
    }

    private function assertSuccessResponse(ResponseInterface $response)
    {
        $responseBody = (string)$response->getBody();
        $parsedResponseBody = json_decode($responseBody, true);
        Assert::assertSame(
            200,
            $response->getStatusCode(),
            "Response http status code does not match. Response body was {$responseBody}"
        );
        Assert::assertSame(
            ['application/json;charset=utf-8'],
            $response->getHeader('Content-Type'),
            "Response header Content-Type is not json. Response body was {$responseBody}"
        );

        $this->assertArrayHasKey('data', $parsedResponseBody);
        $this->assertArrayHasKey('id', $parsedResponseBody['data']);
        $this->assertArrayHasKey('first-name', $parsedResponseBody['data']);
        $this->assertArrayHasKey('last-name', $parsedResponseBody['data']);
        $this->assertArrayHasKey('city', $parsedResponseBody['data']);
        $this->assertArrayHasKey('country', $parsedResponseBody['data']);
        $this->assertArrayHasKey('dateCreated', $parsedResponseBody['data']);
        $this->assertArrayHasKey('userStatus', $parsedResponseBody['data']);
        $this->assertArrayHasKey('userFriends', $parsedResponseBody['data']);

    }

    /**
     * @param int $exceptionCode
     * @param ResponseInterface $response
     */
    private function assertErrorResponse(int $exceptionCode, ResponseInterface $response)
    {
        $responseBody = (string)$response->getBody();
        $parsedResponseBody = json_decode($responseBody, true);
        Assert::assertSame(
            400,
            $response->getStatusCode(),
            "Response http status code does not match. Response body was {$responseBody}"
        );
        Assert::assertSame(
            ['application/json;charset=utf-8'],
            $response->getHeader('Content-Type'),
            "Response header Content-Type is not json. Response body was {$responseBody}"
        );

        $this->assertArrayHasKey('error', $parsedResponseBody);
        $this->assertArrayHasKey('message', $parsedResponseBody['error']);
        $this->assertArrayHasKey('code', $parsedResponseBody['error']);
        $this->assertEquals($exceptionCode, $parsedResponseBody['error']['code']);
    }
}
