<?php

namespace VStancescu\Users\User\Api\ResponseBuilder;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder;
use VStancescu\Users\Entity\User;

/**
* @covers \VStancescu\Users\User\Api\ResponseBuilder\UserApiJsonResponseBuilder
*/
class UserApiJsonResponseBuilderTest extends TestCase
{
    /** @var UserApiJsonResponseBuilder */
    private $sut;

    /** @var JsonErrorApiResponseBuilder | \PHPUnit_Framework_MockObject_MockObject */
    private $jsonErrorResponseBuilderMock;

    protected function setUp()
    {
        $this->jsonErrorResponseBuilderMock = $this->createMock(JsonErrorApiResponseBuilder::class);
        $this->sut = new UserApiJsonResponseBuilder($this->jsonErrorResponseBuilderMock);
    }

    public function testBuildSuccessResponseSuccess()
    {
        $userId = 1;
        $firstName = 'Valentin';
        $lastName = 'Stancescu';
        $email = 'sv1@mailinator.com';
        $description = 'Short description';
        $city = 'Bucharest';
        $country = 'RO';


        $expectedUser = $this->getMockBuilder(User::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$firstName, $lastName, $email, $description, $city, $country])
            ->setMethods(['getId'])
            ->getMock();

        $expectedUser->expects($this->once())
            ->method('getId')
            ->willReturn($userId);

        $jsonResponse = $this->sut->successResponse($expectedUser);

        $this->assertEquals(200, $jsonResponse->getStatusCode());
        $this->assertSame(
            ['application/json;charset=utf-8'],
            $jsonResponse->getHeader('Content-Type'),
            "Response header Content-Type is not json."
        );

        $responseBody = (string)$jsonResponse->getBody();
        $this->assertJson($responseBody);
        $parsedResponseBody = json_decode($responseBody, true);

        $this->assertArrayHasKey('data', $parsedResponseBody);
        $this->assertArrayHasKey('id', $parsedResponseBody['data']);
        $this->assertArrayHasKey('first-name', $parsedResponseBody['data']);
        $this->assertArrayHasKey('last-name', $parsedResponseBody['data']);
        $this->assertArrayHasKey('city', $parsedResponseBody['data']);
        $this->assertArrayHasKey('country', $parsedResponseBody['data']);
        $this->assertArrayHasKey('dateCreated', $parsedResponseBody['data']);
        $this->assertArrayHasKey('userStatus', $parsedResponseBody['data']);
        $this->assertArrayHasKey('userFriends', $parsedResponseBody['data']);
    }

    public function testBuildClientErrorResponseForwardsCallToCommonClass()
    {
        $httpStatus = 400;
        $errorCode = ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST;
        $errorMessage = ApiResponseErrors::ERRORS[ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST];
        $expectedResponse = $this->createMock(ResponseInterface::class);

        $this->jsonErrorResponseBuilderMock->expects($this->once())
            ->method('errorResponse')
            ->with(
                $this->identicalTo($httpStatus),
                $this->identicalTo($errorCode),
                $this->identicalTo($errorMessage)
            )
            ->willReturn($expectedResponse);

        $actualResponse = $this->sut->clientErrorResponse($errorCode);

        $this->assertSame(
            $expectedResponse,
            $actualResponse,
            'The returned Response should be the same coming from the JsonErrorApiResponseBuilder'
        );
    }
}
