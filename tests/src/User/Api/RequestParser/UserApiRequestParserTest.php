<?php

namespace VStancescu\Users\User\Api\RequestParser;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\User\Service\UserFilter;
use VStancescu\Users\User\Service\UserFilterBuilder;
use VStancescu\Users\User\Service\UsersToConnect;
use VStancescu\Users\User\Service\UserToCreate;

/**
 * @covers \VStancescu\Users\User\Api\RequestParser\UserApiRequestParser
 */
class UserApiRequestParserTest extends TestCase
{
    const VALID_FIRST_NAME = 'Valentin';
    const VALID_LAST_NAME = 'Stancescu';
    const VALID_EMAIL = 'sv1@mailinator.com';
    const VALID_DESCRIPTION = 'Short user description';
    const VALID_CITY = 'Bucharest';
    const VALID_COUNTRY = 'RO';

    /** @var UserApiRequestParser */
    private $sut;

    /** @var UserFilterBuilder | | \PHPUnit_Framework_MockObject_MockObject */
    private $userFilterBuilderMock;

    protected function setUp()
    {
        $this->userFilterBuilderMock = $this->createMock(UserFilterBuilder::class);
        $this->sut = new UserApiRequestParser($this->userFilterBuilderMock);
    }

    /**
     * @dataProvider providerSuccessParseCreateRequest
     * @param string $requestBody
     * @param UserToCreate $expectedUserToCreate
     * @throws UserApiRequestParserException
     */
    public function testParseCreateRequestSuccess(string $requestBody, UserToCreate $expectedUserToCreate)
    {
        $requestStub = $this->getRequestStub('getBody', $requestBody);

        $parsedUserToCreate = $this->sut->parseCreateRequest($requestStub);

        $this->assertEquals($expectedUserToCreate, $parsedUserToCreate, 'The request was not parsed correctly');
    }

    /**
     * @dataProvider providerExceptionsParserCreateRequest
     * @param string $requestBody
     * @param string $expectedExceptionCode
     * @throws UserApiRequestParserException
     */
    public function testParserCreateRequestThrowsException(
        string $requestBody,
        string $expectedExceptionCode
    )
    {
        $this->expectException(UserApiRequestParserException::class);
        $this->expectExceptionMessage($expectedExceptionCode);

        $requestStub = $this->getRequestStub('getBody', $requestBody);
        $this->sut->parseCreateRequest($requestStub);
    }

    public function testParseConnectUsersRequestSuccess()
    {
        $userId = rand(1, 50);
        $connectedUserId = rand(51, 100);
        $expectedUserToConnect = new UsersToConnect($userId, $connectedUserId);

        $parsedUserToConnect = $this->sut->parseConnectUsersRequest($userId, $connectedUserId);

        $this->assertEquals($expectedUserToConnect, $parsedUserToConnect);
    }

    public function testParseConnectUsersRequestThrowsException()
    {
        $userId = 'invalid';
        $connectedUserId = 'asd123asd';

        $this->expectException(UserApiRequestParserException::class);
        $this->expectExceptionMessage((string)ApiResponseErrors::USER_ID_NOT_NUMERIC);

        $this->sut->parseConnectUsersRequest($userId, $connectedUserId);
    }

    /** @dataProvider providerGetUserFilterSuccess
     * @param int $userId
     * @param array $requestBody
     * @param UserFilter $expectedUserFilter
     * @throws UserApiRequestParserException
     */
    public function testGetUserRequestSuccess(int $userId, array $requestBody, UserFilter $expectedUserFilter)
    {
        $requestStub = $this->getRequestStub('getQueryParams', $requestBody);

        $this->userFilterBuilderMock->expects($this->once())->method('build')
            ->with($requestBody, $userId)
            ->willReturn($expectedUserFilter);

        $parsedUserFilter = $this->sut->parseGetUserRequest($requestStub, $userId);

        $this->assertEquals($expectedUserFilter, $parsedUserFilter);
    }

    /**
     * @dataProvider providerGetUserFilterExceptions
     * @param int|string $userId
     * @param array $requestBody
     * @param $expectedExceptionCode
     * @throws UserApiRequestParserException
     */
    public function testGetUserRequestThrowsException($userId, array $requestBody, $expectedExceptionCode)
    {
        $this->expectException(UserApiRequestParserException::class);
        $this->expectExceptionMessage($expectedExceptionCode);

        $requestStub = $this->getRequestStub('getQueryParams', $requestBody);

        $this->sut->parseGetUserRequest($requestStub, $userId);
    }

    public function providerGetUserFilterExceptions()
    {
        return [
            'invalid user id' => [
                'asd',
                [],
                (string)ApiResponseErrors::USER_ID_NOT_NUMERIC,
            ],
            'invalid show friends' => [
                '1',
                [
                    'show-friends' => 'invalid',
                    'country' => 'RO',
                    'same-group' => 'true'
                ],
                (string)ApiResponseErrors::USER_INVALID_SHOW_FRIENDS,
            ],
            'invalid same group' => [
                '1',
                [
                    'show-friends' => 'true',
                    'same-group' => 'invalid'
                ],
                (string)ApiResponseErrors::USER_INVALID_SAME_GROUP,
            ],
        ];
    }

    public function providerGetUserFilterSuccess()
    {
        $userId = '1';
        $expectedFullUserFilter = new UserFilter((int)$userId);
        $expectedFullUserFilter->setShowFriends(true);
        $expectedFullUserFilter->setCountry('RO');
        $expectedFullUserFilter->setSameGroup(true);

        $expectedShowFriendsUserFilter = new UserFilter($userId);
        $expectedShowFriendsUserFilter->setShowFriends(true);

        $expectedSameCountryUserFilter = new UserFilter($userId);
        $expectedSameCountryUserFilter->setCountry('RO');

        return [
            'no query params' => [
                $userId,
                [],
                new UserFilter(1)
            ],
            'all query params' => [
                $userId,
                [
                    'show-friends' => 'true',
                    'country' => 'RO',
                    'same-group' => 'true'
                ],
                $expectedFullUserFilter
            ],
            'show friends params' => [
                $userId,
                [
                    'show-friends' => 'true',
                ],
                $expectedShowFriendsUserFilter
            ],
            'same country params' => [
                $userId,
                [
                    'country' => 'RO',
                ],
                $expectedSameCountryUserFilter
            ],

        ];
    }

    public function providerExceptionsParserCreateRequest()
    {
        return [
            'empty request' => [
                '',
                (string)ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST
            ],
            'invalid JSON format' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '"
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST
            ],
            'empty first-name' => [
                '{
                  "first-name": "",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_MISSING_NAME
            ],
            'empty last-name' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_MISSING_NAME
            ],
            'empty email' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_MISSING_EMAIL
            ],
            'invalid email' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "INVALID EMAIL",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_INVALID_EMAIL
            ],
            'empty city' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                (string)ApiResponseErrors::USER_MISSING_CITY
            ],
            'empty country' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": ""
                }',
                (string)ApiResponseErrors::USER_MISSING_COUNTRY
            ],
        ];
    }
    
    public function providerSuccessParseCreateRequest()
    {
        return [
            'list of parameters is valid' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                new UserToCreate(
                    self::VALID_FIRST_NAME,
                    self::VALID_LAST_NAME,
                    self::VALID_EMAIL,
                    self::VALID_DESCRIPTION,
                    self::VALID_CITY,
                    self::VALID_COUNTRY
                ),
            ],
            'different order of parameters is valid' => [
                '{
                  "email": "' . self::VALID_EMAIL . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '"
                }',
                new UserToCreate(
                    self::VALID_FIRST_NAME,
                    self::VALID_LAST_NAME,
                    self::VALID_EMAIL,
                    self::VALID_DESCRIPTION,
                    self::VALID_CITY,
                    self::VALID_COUNTRY
                ),
            ],

            'extra parameters are ignored' => [
                '{
                  "first-name": "' . self::VALID_FIRST_NAME . '",
                  "last-name": "' . self::VALID_LAST_NAME . '",
                  "email": "' . self::VALID_EMAIL . '",
                  "description": "' . self::VALID_DESCRIPTION . '",
                  "city": "' . self::VALID_CITY . '",
                  "country": "' . self::VALID_COUNTRY . '",
                  "EXTRA-FIELD": "extra field value"
                }',
                new UserToCreate(
                    self::VALID_FIRST_NAME,
                    self::VALID_LAST_NAME,
                    self::VALID_EMAIL,
                    self::VALID_DESCRIPTION,
                    self::VALID_CITY,
                    self::VALID_COUNTRY
                ),
            ],
        ];
    }

    /**
     * @param string $methodName
     * @param array|string|null $parsedRequestBody
     * @return \PHPUnit_Framework_MockObject_MockObject | ServerRequestInterface
     */
    private function getRequestStub(string $methodName, $parsedRequestBody)
    {
        $requestStub = $this->createMock(ServerRequestInterface::class);
        $requestStub->method($methodName)->willReturn($parsedRequestBody);
        return $requestStub;
    }
}
