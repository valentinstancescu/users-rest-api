<?php

namespace VStancescu\Users\DI;

use DI\Container;
use DI\ContainerBuilder;

class ContainerHelper
{
    /** @var Container */
    private static $container;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public static function get($name)
    {
        return self::getInstance()->get($name);
    }

    /**
     * @param string $name
     * @param array $parameters
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public static function make($name, array $parameters = [])
    {
        return self::getInstance()->make($name, $parameters);
    }

    /**
     * @return Container
     */
    private static function getInstance()
    {
        if (self::$container === null) {
            self::$container = self::createContainer();
        }

        return self::$container;
    }

    /**
     * @return Container
     */
    public static function createContainer()
    {
        $containerBuilder = new ContainerBuilder();

        self::configureContainerBuilder($containerBuilder);

        return $containerBuilder->build();
    }

    /**
     * @param ContainerBuilder $containerBuilder
     */
    public static function configureContainerBuilder(ContainerBuilder $containerBuilder)
    {
        $containerBuilder->addDefinitions(dirname(dirname(__DIR__)) . '/config/di-config.php');
        $containerBuilder->addDefinitions(dirname(dirname(__DIR__)) . '/config/doctrine-config.php');
        $containerBuilder->addDefinitions(dirname(dirname(__DIR__)) . '/config/local.php');
    }
}
