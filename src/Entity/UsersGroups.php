<?php

namespace VStancescu\Users\Entity;


use DateTime;

class UsersGroups
{
    /** @var integer */
    private $id;

    /** @var User */
    private $user;

    /** @var Group */
    private $group;

    /** @var boolean */
    private $isAdmin;

    /** @var \DateTime */
    private $dateCreated;

    /**
     * UsersGroups constructor.
     * @param User $user
     * @param Group $group
     * @param bool $isAdmin
     */
    public function __construct(User $user, Group $group, bool $isAdmin)
    {
        $this->user = $user;
        $this->group = $group;
        $this->isAdmin = $isAdmin;
        $this->dateCreated = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated(): DateTime
    {
        return $this->dateCreated;
    }

}
