<?php

namespace VStancescu\Users\Entity;

use DateTime;

class Group
{

    /** @var integer */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** string */
    private $city;

    /** @var string should be country code RO, NL, TR, UK */
    private $country;

    /** @var User[] */
    private $users;

    /** @var \DateTime */
    private $dateCreated;

    /**
     * Group constructor.
     * @param string $name
     * @param string $description
     * @param $city
     * @param string $country
     */
    public function __construct(string $name, string $description, $city, string $country)
    {
        $this->name = $name;
        $this->description = $description;
        $this->city = $city;
        $this->country = $country;
        $this->dateCreated = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

}
