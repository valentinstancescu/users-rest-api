<?php

namespace VStancescu\Users\Entity;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

class User
{
    const USER_STATUS_ACTIVE = 'ACTIVE';
    const USER_STATUS_INACTIVE = 'INACTIVE';

    /** @var integer */
    private $id;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $email;

    /** @var string */
    private $description;

    /** @var string */
    private $city;

    /** @var string should be country code RO, NL, TR, UK */
    private $country;

    /** @var \DateTime */
    private $dateCreated;

    /** @var string should be ACTIVE|INACTIVE */
    private $userStatus;

    /** @var ArrayCollection */
    private $friendsWithMe;

    /** @var ArrayCollection */
    private $myFriends;

    /** @var Group[] */
    private $groups;

    /**
     * User constructor.
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $description
     * @param string $city
     * @param string $country
     */
    public function __construct(string $firstName, string $lastName, string $email, string $description, string $city, string $country)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->description = $description;
        $this->city = $city;
        $this->country = $country;
        $this->dateCreated = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $this->userStatus = self::USER_STATUS_ACTIVE;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @return string
     */
    public function getUserStatus(): string
    {
        return $this->userStatus;
    }

    /**
     * @param string $userStatus
     */
    public function setUserStatus(string $userStatus): void
    {
        $this->userStatus = $userStatus;
    }

    /**
     * @return User[]
     */
    public function getFriendsWithMe(): array
    {
        if (!empty($this->friendsWithMe)) {
            return $this->friendsWithMe->toArray();
        }

        return [];
    }

    /**
     * @param User $friendsWithMe
     */
    public function addFriendsWithMe(User $friendsWithMe): void
    {
        $this->friendsWithMe[] = $friendsWithMe;
    }

    /**
     * ATTENTION do not persist entity if you used this method. Original friends will be removed
     * @param ArrayCollection $friendsWithMe
     */
    public function replaceFriendsWithMe(ArrayCollection $friendsWithMe): void
    {
        $this->friendsWithMe = $friendsWithMe;
    }

    /**
     * @return Group[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

}
