<?php

namespace VStancescu\Users\Smarty;

use Smarty;

/**
 * Class SmartyFactory - wrapper class that initialize Smarty engine
 */
class SmartyFactory
{
    /**
     * SmartyTemplate constructor.
     */
    public static function getTemplateEngine()
    {
        $smarty = new Smarty();
        $smarty->setTemplateDir(dirname(__DIR__).'/../templates');
        $smarty->setCompileDir(dirname(__DIR__).'/../templates_c');
        $smarty->setCacheDir(dirname(__DIR__).'/../cache');
        $smarty->setConfigDir(dirname(__DIR__).'/../configs');

        return $smarty;
    }

}
