<?php

namespace VStancescu\Users\Smarty;

use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Group\Service\GroupService;
use VStancescu\Users\Group\Service\GroupServiceException;
use VStancescu\Users\User\Service\UserFilterBuilder;
use VStancescu\Users\User\Service\UserService;
use VStancescu\Users\User\Service\UserServiceException;

/**
 * Class UserDetailsController displays data about a user on front end
 */
class UserDetailsController
{
    const TEMPLATE_FILE = 'user-details.tpl';

    /** @var \Smarty */
    private $smartyEngine;

    /** @var UserService */
    private $userService;

    /** @var GroupService */
    private $groupService;

    /** @var UserFilterBuilder */
    private $userFilterBuilder;

    /**
     * UserDetailsController constructor.
     * @param UserService $userService
     * @param GroupService $groupService
     * @param UserFilterBuilder $userFilterBuilder
     */
    public function __construct(
        UserService $userService,
        GroupService $groupService,
        UserFilterBuilder $userFilterBuilder
    ) {
        $this->userService = $userService;
        $this->groupService = $groupService;
        $this->userFilterBuilder = $userFilterBuilder;

        $smartyTemplate = new SmartyFactory();
        $this->smartyEngine = $smartyTemplate::getTemplateEngine();
    }


    public function display(array $serverRequest): void
    {

        try {
            $this->validateRequest($serverRequest);
        } catch (\SmartyException $exception) {
            $this->smartyEngine->assign('errorMessage', $exception->getMessage());
        }

        $userId = (int)$serverRequest['id'];
        $userFilter = $this->userFilterBuilder->build($serverRequest, $userId);

        try {
            $userDetails = $this->userService->getUserDetails($userFilter);
            $userGroupsIds = $this->userService->getUserGroupsIds($userId);

            $groupsDetails = [];
            foreach ($userGroupsIds as $groupsId) {
                $groupsDetails[] = $this->groupService->getGroupDetails($groupsId);
            }

            $this->smartyEngine->assign('user', $userDetails);
            $this->smartyEngine->assign('userGroups', $groupsDetails);

        } catch (UserServiceException $exception) {
            $this->smartyEngine->assign('errorMessage', $exception->getMessage());
        } catch (GroupServiceException $exception) {
            $this->smartyEngine->assign('errorMessage', $exception->getMessage());
        }

        try {
            $this->smartyEngine->display(self::TEMPLATE_FILE);
        } catch (\Exception $exception) {
            //nothing to show, smarty exception
        }

        return;
    }

    /**
     * @param array $serverRequest
     * @throws \SmartyException
     */
    private function validateRequest(array $serverRequest)
    {

        if (!is_numeric($serverRequest['id'])) {
            throw new \SmartyException(ApiResponseErrors::USER_ID_NOT_NUMERIC);
        }

        if (array_key_exists('show-friends', $serverRequest) && $serverRequest['show-friends'] !== 'true') {
            throw new \SmartyException(ApiResponseErrors::USER_INVALID_SHOW_FRIENDS);
        }

        if (array_key_exists('country', $serverRequest) && empty($serverRequest['country'])) {
            throw new \SmartyException(ApiResponseErrors::USER_MISSING_COUNTRY);
        }

        if (array_key_exists('same-group', $serverRequest) && $serverRequest['same-group'] !== 'true') {
            throw new \SmartyException(ApiResponseErrors::USER_INVALID_SAME_GROUP);
        }
    }

}
