<?php

namespace VStancescu\Users\Smarty;

use VStancescu\Users\User\Service\UserService;
use VStancescu\Users\User\Service\UserServiceException;

/**
 * Class InterfaceController displays data on homepage of the interface
 */
class InterfaceController
{

    const TEMPLATE_FILE = 'interface.tpl';

    /** @var \Smarty */
    private $smartyEngine;

    /** @var UserService */
    private $userService;

    /**
     * InterfaceController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        $smartyTemplate = new SmartyFactory();
        $this->smartyEngine = $smartyTemplate::getTemplateEngine();
    }

    /**
     * Method displays data about created users on the front end
     */
    public function display(): void
    {
        $usersCollection = [];

        try {
            $usersCollection = $this->userService->getUsersCollection();
        } catch (UserServiceException $exception) {
            $this->smartyEngine->assign('errorMessage', $exception->getMessage());
        }

        $this->smartyEngine->assign('usersCollection', $usersCollection);

        try {
            $this->smartyEngine->display(self::TEMPLATE_FILE);
        } catch (\Exception $exception) {
            //nothing to show, smarty exception
        }

        return ;
    }


}
