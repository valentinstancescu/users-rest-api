<?php

namespace VStancescu\Users\Repository;


use Doctrine\ORM\EntityRepository;
use VStancescu\Users\Entity\UsersGroups;
use VStancescu\Users\Repository\Exceptions\UsersGroupsStorageException;

/**
 * Class UsersGroupsRepository

 * This class calls Doctrine-specific services and wraps Doctrine exceptions into Storage Interface exceptions.
 * The instantiation of the Repository classes is done through the Doctrine EntityManager class which, on its turn,
 * is also instantiated through another class, the ObjectManagerFactory located in the common Doctrine package.
 */
class UsersGroupsRepository extends EntityRepository
{

    /**
     * @param UsersGroups $usersGroups
     *
     * @return void
     * @throws UsersGroupsStorageException
     */
    public function saveUsersGroups(UsersGroups $usersGroups): void
    {
        try {
            $this->getEntityManager()->persist($usersGroups);
            $this->getEntityManager()->flush();

        } catch (\Exception $exception) {
            throw new UsersGroupsStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param int $userId
     * @return array
     *
     * @throws UsersGroupsStorageException
     */
    public function getUserGroupsIds(int $userId): array
    {
        try {
            $queryBuilder = $this->getEntityManager()->createQueryBuilder();
            $queryBuilder->select(['(ug.group)'])
                ->from('VStancescu\Users\Entity\UsersGroups', 'ug')
                ->where('ug.user = ' . $userId); //@TODO proper parameter set, doctrine style

            $query = $queryBuilder->getQuery();
            $userGroups = $query->getArrayResult(); //@TODO don't managed to work with getResultMethod()

            $groupIds = [];
            foreach ($userGroups as $group) {
                $groupIds[] = (int)$group[1];
            }

            return $groupIds;
        } catch (\Exception $exception) {
            throw new UsersGroupsStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }


    }

    /**
     * @param int $userId
     * @param array $userGroupsIds
     * @param array $friendsWithMeIds
     *
     * @return array
     * @throws UsersGroupsStorageException
     */
    public function getFriendsFromSameGroups(int $userId, array $userGroupsIds, array $friendsWithMeIds): array
    {
        try {

            $queryBuilder = $this->getEntityManager()->createQueryBuilder();
            $queryBuilder->select('DISTINCT(ug.user)')
                ->from('VStancescu\Users\Entity\UsersGroups', 'ug')
                ->where('ug.group IN (' . implode(", ", $userGroupsIds) .') AND ug.user <> ' . $userId)
                ->andWhere('ug.user IN (' . implode(", ", $friendsWithMeIds) . ')')
                ;

            $query = $queryBuilder->getQuery();

            $friendsFromSameGroup = $query->getArrayResult(); //@TODO don't managed to work with getResultMethod()

            $friendsFromSameGroupArray = [];
            foreach ($friendsFromSameGroup as $group) {
                $friendsFromSameGroupArray[] = (int)$group[1];
            }

            return $friendsFromSameGroupArray;
        } catch (\Exception $exception) {
            throw new UsersGroupsStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
