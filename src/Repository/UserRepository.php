<?php

namespace VStancescu\Users\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityRepository;
use VStancescu\Users\Entity\User;
use VStancescu\Users\Repository\Exceptions\UserDuplicatedStorageException;
use VStancescu\Users\Repository\Exceptions\UserNotFoundStorageException;
use VStancescu\Users\Repository\Exceptions\UserStorageException;


/**
 * Class UserRepository

 * This class calls Doctrine-specific services and wraps Doctrine exceptions into Storage Interface exceptions.
 * The instantiation of the Repository classes is done through the Doctrine EntityManager class which, on its turn,
 * is also instantiated through another class, the ObjectManagerFactory located in the common Doctrine package.
 */
class UserRepository extends EntityRepository
{

    /**
     * @param User $user
     *
     * @return void
     * @throws UserStorageException
     * @throws UserDuplicatedStorageException
     */
    public function saveUser(User $user): void
    {
        try {
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new UserDuplicatedStorageException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );

        } catch (\Exception $exception) {
            throw new UserStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param int $userId
     *
     * @return User
     * @throws UserNotFoundStorageException
     * @throws UserStorageException
     */
    public function getUser(int $userId): User
    {
        try {
            $user = $this->findBy(['id' => $userId]);

        } catch (\Exception $exception) {
            throw new UserStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }

        if (count($user) === 0) {
            throw new UserNotFoundStorageException("User with id '{$userId}' not found in storage");
        }

        return $user[0];
    }

    /**
     * @return array
     * @throws UserStorageException
     */
    public function getUsersCollection()
    {
        try {
            $userCollection = $this->findAll();

        } catch (\Exception $exception) {
            throw new UserStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $userCollection;
    }
}
