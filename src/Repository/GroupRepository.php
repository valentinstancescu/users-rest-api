<?php

namespace VStancescu\Users\Repository;

use Doctrine\ORM\EntityRepository;
use VStancescu\Users\Entity\Group;
use VStancescu\Users\Repository\Exceptions\GroupNotFoundStorageException;
use VStancescu\Users\Repository\Exceptions\GroupStorageException;

/**
 * Class GroupRepository

 * This class calls Doctrine-specific services and wraps Doctrine exceptions into Storage Interface exceptions.
 * The instantiation of the Repository classes is done through the Doctrine EntityManager class which, on its turn,
 * is also instantiated through another class, the ObjectManagerFactory located in the common Doctrine package.
 */
class GroupRepository extends EntityRepository
{

    /**
     * @param Group $group
     *
     * @return void
     * @throws GroupStorageException
     */
    public function saveGroup(Group $group): void
    {
        try {
            $this->getEntityManager()->persist($group);
            $this->getEntityManager()->flush();

        } catch (\Exception $exception) {
            throw new GroupStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param int $groupId
     *
     * @return Group
     * @throws GroupNotFoundStorageException
     * @throws GroupStorageException
     */
    public function getGroup(int $groupId): Group
    {
        try {
            $groupCollection = $this->findBy(['id' => $groupId]);

        } catch (\Exception $exception) {
            throw new GroupStorageException($exception->getMessage(), $exception->getCode(), $exception);
        }

        if (count($groupCollection) === 0) {
            throw new GroupNotFoundStorageException("Group with id '{$groupId}' not found in storage");
        }

        return $groupCollection[0];
    }
}
