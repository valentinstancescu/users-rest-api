<?php

namespace VStancescu\Users\Doctrine;

use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use VStancescu\Users\Common\Config;

class ObjectManagerFactory
{
    /**
     * @param Config $config
     * @return EntityManager
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function createObjectManager(Config $config)
    {
        $doctrineConfig = new Configuration();

        $isDevMode = $config->get('developmentMode') ? $config->get('developmentMode') : false;

        $this->configureCaching($doctrineConfig, $isDevMode);
        $this->configureProxy($doctrineConfig, $isDevMode);
        $this->configureEntitiesMapping($doctrineConfig);

        return EntityManager::create($config->get('connection'), $doctrineConfig);
    }

    /**
     * @param Configuration $config
     * @param bool $isDevMode
     */
    private function configureCaching(Configuration $config, $isDevMode)
    {
        if ($isDevMode) {
            $cache = new ArrayCache();
        } else {
            $cache = new ApcuCache();
        }

        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->setResultCacheImpl($cache);
        $config->setHydrationCacheImpl($cache);
    }

    /**
     * @param Configuration $config
     * @param bool $isDevMode
     */
    private function configureProxy(Configuration $config, $isDevMode)
    {
        $proxyDir = dirname(__DIR__) . '/Doctrine/ProxyEntities';
        $proxyNamespace = 'VStancescu\Users\ProxyEntities';

        $config->setProxyDir($proxyDir);
        $config->setProxyNamespace($proxyNamespace);
        $config->setAutoGenerateProxyClasses($isDevMode);
    }

    /**
     * @param Configuration $config
     * @throws \Doctrine\DBAL\DBALException
     */
    private function configureEntitiesMapping(Configuration $config)
    {
        $driverImpl = new SimplifiedXmlDriver(
            [
                dirname(__DIR__) . '/Doctrine/Config' => 'VStancescu\Users\Entity',
            ]
        );

        $config->setMetadataDriverImpl($driverImpl);

        $config->setNamingStrategy(new UnderscoreNamingStrategy());
    }
}
