<?php

namespace VStancescu\Users\Common\Api;

/**
 * Class ApiResponseErrors
 * This class is responsible with holding a mapping of error codes and messages which are returned
 * from all the APIs (Group, User).
 * All the error messages from the APIs should be found here.
 * In this way we keep together all the logic regarding API error messages.
 * This class is intended to be used only from the API package.
 */
class ApiResponseErrors
{
    // Authentication errors
    const AUTHENTICATION_GENERIC_ERROR  = 40001;

    // API Error Handler errors
    const APPLICATION_INTERNAL_ERROR    = 50500;
    const APPLICATION_PHP_ERROR         = 50501;
    const APPLICATION_NOT_FOUND_ERROR   = 40404;
    const APPLICATION_NOT_ALLOWED_ERROR = 40405;

    //User API specific errors
    const USER_INVALID_FORMAT_OR_EMPTY_REQUEST = 14001;
    const USER_MISSING_NAME = 14002;
    const USER_MISSING_EMAIL = 14003;
    const USER_MISSING_CITY = 14004;
    const USER_MISSING_COUNTRY = 14005;
    const USER_DUPLICATED_ERROR = 14010;
    const USER_ID_NOT_NUMERIC = 14011;
    const NOT_FOUND_USER_ERROR = 14012;
    const USER_ALREADY_CONNECTED_ERROR = 14013;
    const USER_INVALID_SHOW_FRIENDS = 14014;
    const USER_INVALID_SAME_GROUP = 14015;
    const USER_INVALID_EMAIL = 14016;
    const USER_INVALID_COUNTRY = 14017;

    //Group API specific errors
    const GROUP_INVALID_FORMAT_OR_EMPTY_REQUEST = 24001;
    const GROUP_MISSING_NAME = 24002;
    const GROUP_MISSING_CITY = 24003;
    const GROUP_MISSING_COUNTRY = 24004;
    const GROUP_MISSING_FOUNDER = 24005;
    const GROUP_ID_NOT_NUMERIC = 24006;
    const GROUP_FOUNDER_MUST_BE_INTEGER = 24007;
    const NOT_FOUND_GROUP_ERROR = 24012;
    const GROUP_INVALID_COUNTRY = 24013;


    const ERRORS = [

        // Authentication errors
        self::AUTHENTICATION_GENERIC_ERROR  => "Access denied!",

        // API Error Handler errors
        self::APPLICATION_INTERNAL_ERROR    => 'Application error',
        self::APPLICATION_PHP_ERROR         => 'Internal application error',
        self::APPLICATION_NOT_FOUND_ERROR   => 'Not found',
        self::APPLICATION_NOT_ALLOWED_ERROR => 'Method not allowed. HTTP method must be: %s',

        //User API specific errors
        self::USER_INVALID_FORMAT_OR_EMPTY_REQUEST => 'Invalid format or empty request',
        self::USER_MISSING_NAME => 'Missing parameter `name` in request',
        self::USER_MISSING_EMAIL => 'Missing parameter `email` in request',
        self::USER_MISSING_CITY => 'Missing parameter `city` in request',
        self::USER_MISSING_COUNTRY => 'Missing parameter `country` in request',
        self::USER_DUPLICATED_ERROR => 'User with this email already exists',
        self::USER_ID_NOT_NUMERIC => 'User ids sent, are not numeric values',
        self::NOT_FOUND_USER_ERROR => 'User not found in system',
        self::USER_ALREADY_CONNECTED_ERROR => 'Users are already connected',
        self::USER_INVALID_SHOW_FRIENDS => 'Parameter `show-friends` should have value `true`',
        self::USER_INVALID_SAME_GROUP => 'Parameter `same-group` should have value `true`',
        self::USER_INVALID_EMAIL => 'Email sent has an invalid format',
        self::USER_INVALID_COUNTRY => 'Country has to be the country code: RO, NL',

        //Group API specific errors
        self::GROUP_INVALID_FORMAT_OR_EMPTY_REQUEST => 'Invalid format or empty request',
        self::GROUP_MISSING_NAME => 'Missing parameter `name` in request',
        self::GROUP_MISSING_CITY => 'Missing parameter `city` in request',
        self::GROUP_MISSING_COUNTRY => 'Missing parameter `country` in request',
        self::GROUP_MISSING_FOUNDER => 'Missing parameter `foundedBy` in request',
        self::GROUP_ID_NOT_NUMERIC => 'Group id must be numeric',
        self::GROUP_FOUNDER_MUST_BE_INTEGER => 'Parameter `foundedBy` must be integer',
        self::NOT_FOUND_GROUP_ERROR => 'Group not found in system',
        self::GROUP_INVALID_COUNTRY => 'Country has to be the country code: RO, NL',
    ];



}
