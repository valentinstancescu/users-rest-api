<?php

namespace VStancescu\Users\Common\Api\ResponseBuilder;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

/**
 * Class JsonErrorApiResponseBuilder
 * This class is responsible with building an error Response with json content-type header and body formatted as json.
 * This class is common for all the APIs because we want to have a uniform error response.
 */
class JsonErrorApiResponseBuilder implements ErrorApiResponseBuilderInterface
{
    /**
     * @param int $httpStatus
     * @param string $errorCode
     * @param string $errorMessage
     * @return ResponseInterface
     */
    public function errorResponse($httpStatus, $errorCode, $errorMessage)
    {
        return (new Response($httpStatus))
            ->withJson(
                [
                    'error' => [
                        'message' => $errorMessage,
                        'code' => (string)$errorCode,
                    ]
                ]
            );
    }
}
