<?php

namespace VStancescu\Users\Common\Api\ResponseBuilder;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ErrorApiResponseBuilderInterface
 * The purpose of this interface is to be agnostic of the content type of the Response.
 * Implementations will format the Response body as json, xml etc.
 * This interface is common for all the APIs because we want to have a uniform error response.
 */
interface ErrorApiResponseBuilderInterface
{
    /**
     * @param int $httpStatus
     * @param string $errorCode
     * @param string $errorMessage
     * @return ResponseInterface
     */
    public function errorResponse($httpStatus, $errorCode, $errorMessage);
}
