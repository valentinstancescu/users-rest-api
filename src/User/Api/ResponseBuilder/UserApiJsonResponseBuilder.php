<?php

namespace VStancescu\Users\User\Api\ResponseBuilder;


use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder;
use VStancescu\Users\Entity\User;

class UserApiJsonResponseBuilder implements UserApiResponseBuilderInterface
{

    /** @var JsonErrorApiResponseBuilder */
    private $jsonErrorResponseBuilder;

    public function __construct(JsonErrorApiResponseBuilder $jsonErrorResponseBuilder)
    {
        $this->jsonErrorResponseBuilder = $jsonErrorResponseBuilder;
    }

    /**
     * @param User $user
     * @return ResponseInterface
     */
    public function successResponse(User $user): ResponseInterface
    {


        return (new Response())
            ->withJson(
                [
                    'data' => $this->formatUserEntity($user),
                ]
            );
    }

    /**
     * @param string $userApiErrorCode
     * @return ResponseInterface
     * This method must return a response code of 400
     */
    public function clientErrorResponse($userApiErrorCode): ResponseInterface
    {
        return $this->jsonErrorResponseBuilder->errorResponse(
            400,
            $userApiErrorCode,
            ApiResponseErrors::ERRORS[$userApiErrorCode]
        );
    }

    /**
     * @param User[] $usersCollection
     *
     * @return ResponseInterface
     */
    public function successResponseCollection(array $usersCollection): ResponseInterface
    {
        return (new Response())
            ->withJson(
                [ 'data' => $this->formatUsersCollection($usersCollection)]
            );
    }

    /**
     * @param array $usersCollection
     *
     * @return array
     */
    private function formatUsersCollection(array $usersCollection): array
    {
        $formattedUsersCollection = [];
        foreach ($usersCollection as $user) {
            $formattedUsersCollection[] = $this->formatUserEntity($user);
        }

        return $formattedUsersCollection;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function formatUserEntity(User $user): array
    {
        $friendsArray = [];
        foreach ($user->getFriendsWithMe() as $friends) {
            $friendsArray[] = [
                'id' => $friends->getId(),
                'email' => $friends->getEmail(),
                'country' => $friends->getCountry(),
            ];
        }

        return [
            'id' => $user->getId(),
            'first-name' => $user->getFirstName(),
            'last-name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'description' => $user->getDescription(),
            'city' => $user->getCity(),
            'country' => $user->getCountry(),
            'dateCreated' => $user->getDateCreated()->format('Y-m-d H:i:s'),
            'userStatus' => $user->getUserStatus(),
            'userFriends' => $friendsArray,
        ];
    }
}
