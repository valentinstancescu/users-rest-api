<?php

namespace VStancescu\Users\User\Api\ResponseBuilder;

use Psr\Http\Message\ResponseInterface;
use VStancescu\Users\Entity\User;

/**
 * Interface UserApiResponseBuilderInterface
 * The purpose of this interface is to be agnostic of the content type of the Response.
 * Implementations will format the Response body as json, xml etc.
 */
interface UserApiResponseBuilderInterface
{
    /**
     * @param User $user
     *
     * @return ResponseInterface
     * This method must return a response code of 200
     */
    public function successResponse(User $user): ResponseInterface;

    /**
     * @param User[] $usersCollection
     * @return ResponseInterface
     */
    public function successResponseCollection(array $usersCollection);

    /**
     * @param string $userApiErrorCode
     *
     * @return ResponseInterface
     * This method must return a response code of 400
     */
    public function clientErrorResponse($userApiErrorCode): ResponseInterface;
}
