<?php

namespace VStancescu\Users\User\Api\RequestParser;


use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\User\Service\UserFilter;
use VStancescu\Users\User\Service\UserFilterBuilder;
use VStancescu\Users\User\Service\UsersToConnect;
use VStancescu\Users\User\Service\UserToCreate;

class UserApiRequestParser
{

    /** @var UserFilterBuilder */
    private $userFilterBuilder;

    /**
     * UserApiRequestParser constructor.
     * @param UserFilterBuilder $userFilterBuilder
     */
    public function __construct(UserFilterBuilder $userFilterBuilder)
    {
        $this->userFilterBuilder = $userFilterBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return UserToCreate
     * @throws UserApiRequestParserException
     */
    public function parseCreateRequest(ServerRequestInterface $request): UserToCreate
    {

        $requestBody = json_decode($request->getBody(), true);

        if (!$requestBody) {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST);
        }

        $this->checkRequiredCreateFields($requestBody);

        return new UserToCreate(
            $requestBody['first-name'],
            $requestBody['last-name'],
            $requestBody['email'],
            $requestBody['description'],
            $requestBody['city'],
            $requestBody['country']
        );
    }

    /**
     * @param string $userId
     * @param string $friendUserId
     *
     * @return UsersToConnect
     * @throws UserApiRequestParserException
     */
    public function parseConnectUsersRequest(string $userId, string $friendUserId): UsersToConnect
    {
        if (!is_numeric($userId) || !is_numeric($friendUserId)) {
            $this->throwParsingException(ApiResponseErrors::USER_ID_NOT_NUMERIC);
        }

        return new UsersToConnect((int)$userId, (int)$friendUserId);
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $userId
     *
     * @return UserFilter
     * @throws UserApiRequestParserException
     */
    public function parseGetUserRequest(ServerRequestInterface $request, string $userId): UserFilter
    {
        $queryParams = $request->getQueryParams();
        $this->checkUserFilterFields($queryParams, $userId);

        return $this->userFilterBuilder->build($queryParams, $userId);
    }

    /**
     * @param $errorCode
     * @throws UserApiRequestParserException
     */
    private function throwParsingException($errorCode)
    {
        throw new UserApiRequestParserException($errorCode);
    }

    /**
     * @param array $requestBody
     *
     * @return void
     * @throws UserApiRequestParserException
     */
    private function checkRequiredCreateFields(array $requestBody): void
    {
        if (!is_array($requestBody) || $requestBody === []) {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_FORMAT_OR_EMPTY_REQUEST);
        }

        if (empty($requestBody['first-name'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_NAME);
        }

        if (empty($requestBody['last-name'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_NAME);
        }

        if (empty($requestBody['email'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_EMAIL);
        }

        if (!filter_var($requestBody['email'], FILTER_VALIDATE_EMAIL)) {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_EMAIL);
        }

        if (empty($requestBody['city'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_CITY);
        }

        if (empty($requestBody['country'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_COUNTRY);
        }

        if (mb_strlen($requestBody['country']) != 2) {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_COUNTRY);
        }

        return;
    }

    /**
     * @param array $filterParams
     * @param string $userId
     *
     * @return UserFilter
     * @throws UserApiRequestParserException
     */
    private function checkUserFilterFields(array $filterParams, string $userId): void
    {
        if (!is_numeric($userId)) {
            $this->throwParsingException(ApiResponseErrors::USER_ID_NOT_NUMERIC);
        }

        if (array_key_exists('show-friends', $filterParams) && $filterParams['show-friends'] !== 'true') {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_SHOW_FRIENDS);
        }

        if (array_key_exists('country', $filterParams) && empty($filterParams['country'])) {
            $this->throwParsingException(ApiResponseErrors::USER_MISSING_COUNTRY);
        }

        if (array_key_exists('same-group', $filterParams) && $filterParams['same-group'] !== 'true') {
            $this->throwParsingException(ApiResponseErrors::USER_INVALID_SAME_GROUP);
        }

        return;
    }
}
