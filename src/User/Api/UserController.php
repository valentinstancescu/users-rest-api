<?php

namespace VStancescu\Users\User\Api;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\User\Api\RequestParser\UserApiRequestParser;
use VStancescu\Users\User\Api\RequestParser\UserApiRequestParserException;
use VStancescu\Users\User\Api\ResponseBuilder\UserApiResponseBuilderInterface;
use VStancescu\Users\User\Service\UserService;
use VStancescu\Users\User\Service\UserServiceException;

class UserController
{

    /** @var UserApiRequestParser */
    private $requestParser;

    /** @var UserApiResponseBuilderInterface */
    private $responseBuilder;

    /** @var UserService */
    private $userService;

    /**
     * UserController constructor.
     * @param UserApiRequestParser $requestParser
     * @param UserApiResponseBuilderInterface $responseBuilder
     * @param UserService $userService
     */
    public function __construct(
        UserApiRequestParser $requestParser,
        UserApiResponseBuilderInterface $responseBuilder,
        UserService $userService
    ) {
        $this->requestParser = $requestParser;
        $this->responseBuilder = $responseBuilder;
        $this->userService = $userService;
    }

    /**
     * This method creates a new user in system
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $userToCreate = $this->requestParser->parseCreateRequest($request);
        } catch (UserApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $user = $this->userService->createUser($userToCreate);

            return $this->responseBuilder->successResponse($user);
        } catch (UserServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }
    }

    /**
     * @param string $userId
     * @param string $friendUserId
     *
     * @return ResponseInterface
     * @throws \VStancescu\Users\Repository\Exceptions\UserDuplicatedStorageException
     */
    public function addFriendToUser(string $userId, string $friendUserId): ResponseInterface
    {
        try {
            $usersToConnect = $this->requestParser->parseConnectUsersRequest($userId, $friendUserId);
        } catch (UserApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $user = $this->userService->connectUsers($usersToConnect);

            return $this->responseBuilder->successResponse($user);
        } catch (UserServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }

    }

    /**
     * @param ServerRequestInterface $request
     * @param string $userId
     *
     * @return ResponseInterface
     */
    public function getUserDetails(ServerRequestInterface $request, string $userId): ResponseInterface
    {
        try {
            $userFilter = $this->requestParser->parseGetUserRequest($request, $userId);
        } catch (UserApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $user = $this->userService->getUserDetails($userFilter);

            return $this->responseBuilder->successResponse($user);
        } catch (UserServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }

    }

    /**
     * @return ResponseInterface
     */
    public function getUsers(): ResponseInterface
    {
        try {
            $usersCollection = $this->userService->getUsersCollection();

            return $this->responseBuilder->successResponseCollection($usersCollection);
        } catch (UserServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }

    }

}
