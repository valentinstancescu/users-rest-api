<?php

namespace VStancescu\Users\User\Service;


class UserServiceException extends \Exception
{
    const DUPLICATE_USER_ERROR = 14010;
    const NOT_FOUND_USER_ERROR = 14012;
    const USER_ALREADY_CONNECTED_ERROR = 14013;

    const ERRORS = [
        self::DUPLICATE_USER_ERROR => 'User with this email already exists',
        self::NOT_FOUND_USER_ERROR => 'User not found in system',
        self::USER_ALREADY_CONNECTED_ERROR => 'Users are already connected',
    ];


}
