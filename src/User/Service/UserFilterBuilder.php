<?php

namespace VStancescu\Users\User\Service;


class UserFilterBuilder
{
    /**
     * @param array $filterParams
     * @param string $userId
     *
     * @return UserFilter
     */
    public function build(array $filterParams, string $userId): UserFilter
    {
        $userFilter = new UserFilter((int)$userId);

        if(!empty($filterParams['show-friends'])) {
            $userFilter->setShowFriends(true);
        }

        if(!empty($filterParams['country'])) {
            $userFilter->setCountry($filterParams['country']);
        }

        if (!empty($filterParams['same-group'])) {
            $userFilter->setSameGroup(true);
        }

        return $userFilter;
    }
}
