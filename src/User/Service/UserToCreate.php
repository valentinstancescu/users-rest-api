<?php

namespace VStancescu\Users\User\Service;

/**
 * Class UserToCreate
 * This class encapsulates the data needed for operations that create and update a User entity.
 * It is an immutable value object.
 * It is required as input for methods in the UserService.
 * This class also acts as a data transfer object from the API layer to the Service layer.
 */
class UserToCreate
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $email;

    /** @var string */
    private $description;

    /** @var string */
    private $city;

    /** @var string should be country code RO, NL, TR, UK */
    private $country;

    /**
     * UserToCreate constructor.
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $description
     * @param string $city
     * @param string $country
     */
    public function __construct(string $firstName, string $lastName, string $email, string $description, string $city, string $country)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->description = $description;
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

}
