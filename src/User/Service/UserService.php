<?php

namespace VStancescu\Users\User\Service;


use Doctrine\Common\Collections\ArrayCollection;
use VStancescu\Users\Entity\User;
use VStancescu\Users\Repository\Exceptions\UserDuplicatedStorageException;
use VStancescu\Users\Repository\Exceptions\UserNotFoundStorageException;
use VStancescu\Users\Repository\Exceptions\UsersGroupsStorageException;
use VStancescu\Users\Repository\Exceptions\UserStorageException;
use VStancescu\Users\Repository\UserRepository;
use VStancescu\Users\Repository\UsersGroupsRepository;

class UserService
{
    /** @var UserRepository */
    private $userRepository;

    /** @var UsersGroupsRepository */
    private $usersGroupsRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param UsersGroupsRepository $usersGroupsRepository
     */
    public function __construct(UserRepository $userRepository, UsersGroupsRepository $usersGroupsRepository)
    {
        $this->userRepository = $userRepository;
        $this->usersGroupsRepository = $usersGroupsRepository;
    }

    /**
     * @param UserToCreate $userToCreate
     *
     * @return User
     * @throws UserServiceException
     */
    public function createUser(UserToCreate $userToCreate): User
    {
        $user = new User(
          $userToCreate->getFirstName(),
          $userToCreate->getLastName(),
          $userToCreate->getEmail(),
          $userToCreate->getDescription(),
          $userToCreate->getCity(),
          $userToCreate->getCountry()
        );

        try {
            $this->userRepository->saveUser($user);

        } catch (UserDuplicatedStorageException $exception) {
            throw new UserServiceException(
                UserServiceException::ERRORS[UserServiceException::DUPLICATE_USER_ERROR],
                UserServiceException::DUPLICATE_USER_ERROR,
                $exception);
        } catch (UserStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $user;

    }

    /**
     * @param UsersToConnect $usersToConnect
     * @return User
     *
     * @throws UserServiceException
     */
    public function connectUsers(UsersToConnect $usersToConnect): User
    {
        try {
            $mainUser = $this->userRepository->getUser($usersToConnect->getUserId());
            $connectionUser = $this->userRepository->getUser($usersToConnect->getConnectedUserId());
            $mainUser->addFriendsWithMe($connectionUser);
            $connectionUser->addFriendsWithMe($mainUser);
            $this->userRepository->saveUser($mainUser);
            $this->userRepository->saveUser($connectionUser);

        } catch (UserNotFoundStorageException $exception) {
            throw new UserServiceException(
                UserServiceException::ERRORS[UserServiceException::DUPLICATE_USER_ERROR],
                UserServiceException::DUPLICATE_USER_ERROR,
                $exception);
        } catch (UserDuplicatedStorageException $exception) {
            throw new UserServiceException(
                UserServiceException::ERRORS[UserServiceException::USER_ALREADY_CONNECTED_ERROR],
                UserServiceException::USER_ALREADY_CONNECTED_ERROR,
                $exception);
        } catch (UserStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $mainUser;
    }

    /**
     * @param UserFilter $userFilter
     *
     * @return User
     * @throws UserServiceException
     */
    public function getUserDetails(UserFilter $userFilter): User
    {
        try {
            $user = $this->userRepository->getUser($userFilter->getUserId());

            $userGroupsIds = $this->usersGroupsRepository->getUserGroupsIds($user->getId());

            $friendsWithMeIds = [];
            $userFilteredFriendsData = [];

            if ($userFilter->isShowFriends()) {
                foreach ($user->getFriendsWithMe() as $friends) {
                    if (!is_null($userFilter->getCountry()) && $userFilter->getCountry() !== $friends->getCountry()) {
                        continue;
                    }

                    $friendsWithMeIds[] = $friends->getId();
                }

                $friendsFromSameGroup = [];
                if ($userFilter->isSameGroup()) {
                    $friendsFromSameGroup = $this->usersGroupsRepository->getFriendsFromSameGroups($user->getId(), $userGroupsIds, $friendsWithMeIds);
                }

                $filteredUserFriendsIds = ($userFilter->isSameGroup()) ? $friendsFromSameGroup : $friendsWithMeIds;

                foreach ($filteredUserFriendsIds as $filteredUserFriendsId) {
                    $userFilteredFriendsData[] = $this->userRepository->getUser($filteredUserFriendsId);
                }
            }

            $user->replaceFriendsWithMe(new ArrayCollection($userFilteredFriendsData));

        } catch (UserNotFoundStorageException $exception) {
            throw new UserServiceException(
                UserServiceException::ERRORS[UserServiceException::NOT_FOUND_USER_ERROR],
                UserServiceException::NOT_FOUND_USER_ERROR,
                $exception);
        } catch (UserStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        } catch (UsersGroupsStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $user;
    }

    /**
     * @param int $userId
     * @return array
     * @throws UserServiceException
     */
    public function getUserGroupsIds(int $userId): array
    {
        try {
            return $this->usersGroupsRepository->getUserGroupsIds($userId);
        } catch (UsersGroupsStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @return array
     * @throws UserServiceException
     */
    public function getUsersCollection()
    {
        try {
            $usersCollection = $this->userRepository->getUsersCollection();
        } catch (UserStorageException $exception) {
            throw new UserServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $usersCollection;
    }

}
