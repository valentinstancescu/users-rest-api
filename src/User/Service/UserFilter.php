<?php

namespace VStancescu\Users\User\Service;


class UserFilter
{
    /** @var integer */
    private $userId;

    /** @var bool */
    private $showFriends = false;

    /** @var string|null */
    private $country;

    /** @var boolean */
    private $sameGroup = false;

    /**
     * UserFilter constructor.
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function isShowFriends(): bool
    {
        return $this->showFriends;
    }

    /**
     * @param bool $showFriends
     */
    public function setShowFriends(bool $showFriends): void
    {
        $this->showFriends = $showFriends;
    }

    /**
     * @return null|string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return bool
     */
    public function isSameGroup(): bool
    {
        return $this->sameGroup;
    }

    /**
     * @param bool $sameGroup
     */
    public function setSameGroup(bool $sameGroup): void
    {
        $this->sameGroup = $sameGroup;
    }

}
