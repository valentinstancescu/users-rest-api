<?php

namespace VStancescu\Users\User\Service;


class UsersToConnect
{
    /** @var integer */
    private $userId;

    /** @var integer */
    private $connectedUserId;

    /**
     * UsersToConnect constructor.
     * @param int $userId
     * @param int $connectedUserId
     */
    public function __construct(int $userId, int $connectedUserId)
    {
        $this->userId = $userId;
        $this->connectedUserId = $connectedUserId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getConnectedUserId(): int
    {
        return $this->connectedUserId;
    }

}
