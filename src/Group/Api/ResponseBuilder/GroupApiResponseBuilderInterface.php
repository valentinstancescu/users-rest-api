<?php

namespace VStancescu\Users\Group\Api\ResponseBuilder;

use Psr\Http\Message\ResponseInterface;
use VStancescu\Users\Entity\Group;


/**
 * Interface GroupApiResponseBuilderInterface
 * The purpose of this interface is to be agnostic of the content type of the Response.
 * Implementations will format the Response body as json, xml etc.
 */
interface GroupApiResponseBuilderInterface
{
    /**
     * @param Group $group
     *
     * @return ResponseInterface
     * This method must return a response code of 200
     */
    public function successResponse(Group $group): ResponseInterface;

    /**
     * @return ResponseInterface
     * This method must return a response code of 200
     */
    public function successSimpleResponse(): ResponseInterface;

    /**
     * @param string $groupApiErrorCode
     *
     * @return ResponseInterface
     * This method must return a response code of 400
     */
    public function clientErrorResponse($groupApiErrorCode): ResponseInterface;
}
