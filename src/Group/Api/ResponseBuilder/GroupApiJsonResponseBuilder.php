<?php

namespace VStancescu\Users\Group\Api\ResponseBuilder;


use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\JsonErrorApiResponseBuilder;
use VStancescu\Users\Entity\Group;

class GroupApiJsonResponseBuilder implements GroupApiResponseBuilderInterface
{

    /** @var JsonErrorApiResponseBuilder */
    private $jsonErrorResponseBuilder;

    public function __construct(JsonErrorApiResponseBuilder $jsonErrorResponseBuilder)
    {
        $this->jsonErrorResponseBuilder = $jsonErrorResponseBuilder;
    }

    /**
     * @param Group $group
     * @return ResponseInterface
     */
    public function successResponse(Group $group): ResponseInterface
    {

        return (new Response())
            ->withJson(
                [
                    'data' => [
                        'id' => $group->getId(),
                        'name' => $group->getName(),
                        'description' => $group->getDescription(),
                        'city' => $group->getCity(),
                        'country' => $group->getCountry(),
                        'dateCreated' => $group->getDateCreated()->format('Y-m-d H:i:s'),
                    ]
                ]
            );
    }

    /**
     * @return ResponseInterface
     * This method must return a response code of 200
     */
    public function successSimpleResponse(): ResponseInterface
    {
        return (new Response())
            ->withJson(
                [
                    'data' => [
                        'message' => 'OK',
                    ]
                ]
            );
    }

    /**
     * @param string $userApiErrorCode
     * @return ResponseInterface
     * This method must return a response code of 400
     */
    public function clientErrorResponse($userApiErrorCode): ResponseInterface
    {
        return $this->jsonErrorResponseBuilder->errorResponse(
            400,
            $userApiErrorCode,
            ApiResponseErrors::ERRORS[$userApiErrorCode]
        );
    }
}
