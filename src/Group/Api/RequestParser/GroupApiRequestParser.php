<?php

namespace VStancescu\Users\Group\Api\RequestParser;


use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Group\Service\GroupToCreate;
use VStancescu\Users\Group\Service\UserToGroup;

class GroupApiRequestParser
{

    /**
     * @param ServerRequestInterface $request
     *
     * @return GroupToCreate
     * @throws GroupApiRequestParserException
     */
    public function parseCreateRequest(ServerRequestInterface $request): GroupToCreate
    {
        $requestBody = json_decode($request->getBody(), true);

        $this->checkRequiredCreateFields($requestBody);

        return new GroupToCreate(
            $requestBody['name'],
            $requestBody['description'],
            $requestBody['city'],
            $requestBody['country'],
            (int) $requestBody['foundedBy']
        );
    }

    /**
     * @param string $userId
     * @param string $groupId
     *
     * @return UserToGroup
     * @throws GroupApiRequestParserException
     */
    public function parseUserToGroupRequest(string $userId, string $groupId): UserToGroup
    {
        if (!is_numeric($userId) || !is_numeric($groupId)) {
            $this->throwParsingException(ApiResponseErrors::GROUP_ID_NOT_NUMERIC);
        }

        return new UserToGroup((int)$userId, (int)$groupId);
    }

    /**
     * @param $groupId
     *
     * @return int
     * @throws GroupApiRequestParserException
     */
    public function parseGroupIdRequest($groupId)
    {
        if (!is_numeric($groupId)) {
            $this->throwParsingException(ApiResponseErrors::GROUP_ID_NOT_NUMERIC);
        }

        return (int)$groupId;
    }

    /**
     * @param array $requestBody
     *
     * @return void
     * @throws GroupApiRequestParserException
     */
    private function checkRequiredCreateFields(array $requestBody): void
    {
        if (!is_array($requestBody) || $requestBody === []) {
            $this->throwParsingException(ApiResponseErrors::GROUP_INVALID_FORMAT_OR_EMPTY_REQUEST);
        }

        if (empty($requestBody['name'])) {
            $this->throwParsingException(ApiResponseErrors::GROUP_MISSING_NAME);
        }

        if (empty($requestBody['city'])) {
            $this->throwParsingException(ApiResponseErrors::GROUP_MISSING_CITY);
        }

        if (empty($requestBody['country'])) {
            $this->throwParsingException(ApiResponseErrors::GROUP_MISSING_COUNTRY);
        }

        if (mb_strlen($requestBody['country']) != 2) {
            $this->throwParsingException(ApiResponseErrors::GROUP_INVALID_COUNTRY);
        }

        if (empty($requestBody['foundedBy'])) {
            $this->throwParsingException(ApiResponseErrors::GROUP_MISSING_FOUNDER);
        }

        if (!is_numeric($requestBody['foundedBy'])) {
            $this->throwParsingException(ApiResponseErrors::GROUP_FOUNDER_MUST_BE_INTEGER);
        }

        return;
    }

    /**
     * @param $errorCode
     * @throws GroupApiRequestParserException
     */
    private function throwParsingException($errorCode)
    {
        throw new GroupApiRequestParserException($errorCode);
    }
}
