<?php

namespace VStancescu\Users\Group\Api;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use VStancescu\Users\Group\Api\RequestParser\GroupApiRequestParser;
use VStancescu\Users\Group\Api\RequestParser\GroupApiRequestParserException;
use VStancescu\Users\Group\Api\ResponseBuilder\GroupApiResponseBuilderInterface;
use VStancescu\Users\Group\Service\GroupService;
use VStancescu\Users\Group\Service\GroupServiceException;

class GroupController
{

    /** @var GroupApiRequestParser */
    private $requestParser;

    /** @var GroupApiResponseBuilderInterface */
    private $responseBuilder;

    /** @var GroupService */
    private $groupService;

    /**
     * GroupController constructor.
     * @param GroupApiRequestParser $requestParser
     * @param GroupApiResponseBuilderInterface $responseBuilder
     * @param GroupService $groupService
     */
    public function __construct(
        GroupApiRequestParser $requestParser,
        GroupApiResponseBuilderInterface $responseBuilder,
        GroupService $groupService
    ) {
        $this->requestParser = $requestParser;
        $this->responseBuilder = $responseBuilder;
        $this->groupService = $groupService;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $groupToCreate = $this->requestParser->parseCreateRequest($request);
        } catch (GroupApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $group = $this->groupService->createGroup($groupToCreate);

            return $this->responseBuilder->successResponse($group);
        } catch (GroupServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }
    }

    /**
     * @param string $groupId
     * @param string $userId
     *
     * @return ResponseInterface
     */
    public function addUserToGroup(string $groupId, string $userId): ResponseInterface
    {

        try {
            $userToGroup = $this->requestParser->parseUserToGroupRequest($userId, $groupId);
        } catch (GroupApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $this->groupService->addUserToGroup($userToGroup);

            return $this->responseBuilder->successSimpleResponse();
        } catch (GroupServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }
    }

    /**
     * @param string $groupId
     *
     * @return ResponseInterface
     */
    public function getGroupDetails(string $groupId): ResponseInterface
    {
        try {
            $groupId = $this->requestParser->parseGroupIdRequest($groupId);
        } catch (GroupApiRequestParserException $exception) {

            return $this->responseBuilder->clientErrorResponse($exception->getMessage());
        }

        try {
            $group = $this->groupService->getGroupDetails($groupId);

            return $this->responseBuilder->successResponse($group);
        } catch (GroupServiceException $exception) {
            return $this->responseBuilder->clientErrorResponse($exception->getCode());
        }
    }

}
