<?php

namespace VStancescu\Users\Group\Service;


class UserToGroup
{

    /** @var integer */
    private $userId;

    /** @var integer */
    private $groupId;

    /**
     * UserToGroup constructor.
     * @param int $userId
     * @param int $groupId
     */
    public function __construct(int $userId, int $groupId)
    {
        $this->userId = $userId;
        $this->groupId = $groupId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

}
