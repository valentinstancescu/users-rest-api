<?php

namespace VStancescu\Users\Group\Service;


class GroupServiceException extends \Exception
{
    const NOT_FOUND_GROUP_ERROR = 24012;

    const ERRORS = [
        self::NOT_FOUND_GROUP_ERROR => 'Group not found in system',
    ];
}
