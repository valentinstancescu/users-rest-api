<?php

namespace VStancescu\Users\Group\Service;


use VStancescu\Users\Entity\Group;
use VStancescu\Users\Entity\UsersGroups;
use VStancescu\Users\Repository\Exceptions\GroupNotFoundStorageException;
use VStancescu\Users\Repository\Exceptions\GroupStorageException;
use VStancescu\Users\Repository\Exceptions\UsersGroupsStorageException;
use VStancescu\Users\Repository\GroupRepository;
use VStancescu\Users\Repository\UsersGroupsRepository;
use VStancescu\Users\User\Service\UserFilter;
use VStancescu\Users\User\Service\UserService;
use VStancescu\Users\User\Service\UserServiceException;

class GroupService
{

    /** @var UserService */
    private $userService;

    /** @var GroupRepository */
    private $groupRepository;

    /** @var UsersGroupsRepository */
    private $usersGroupsRepository;

    /**
     * GroupService constructor.
     * @param UserService $userService
     * @param GroupRepository $groupRepository
     * @param UsersGroupsRepository $usersGroupsRepository
     */
    public function __construct(
        UserService $userService,
        GroupRepository $groupRepository,
        UsersGroupsRepository $usersGroupsRepository
    ) {
        $this->userService = $userService;
        $this->groupRepository = $groupRepository;
        $this->usersGroupsRepository = $usersGroupsRepository;
    }

    /**
     * @param GroupToCreate $groupToCreate
     *
     * @return Group
     * @throws GroupServiceException
     */
    public function createGroup(GroupToCreate $groupToCreate): Group
    {
        try{
            $userFilter = new UserFilter($groupToCreate->getFoundedBy());
            $foundingUser = $this->userService->getUserDetails($userFilter);
        } catch (UserServiceException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        $group = new Group(
            $groupToCreate->getName(),
            $groupToCreate->getDescription(),
            $groupToCreate->getCity(),
            $groupToCreate->getCountry()
        );

        try{
            $this->groupRepository->saveGroup($group);


            $usersGroups = new UsersGroups(
                $foundingUser,
                $group,
                true
            );
            $this->usersGroupsRepository->saveUsersGroups($usersGroups);

        } catch (GroupStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        } catch (UsersGroupsStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $group;
    }


    /**
     * @param UserToGroup $userToGroup
     * @throws GroupServiceException
     */
    public function addUserToGroup(UserToGroup $userToGroup): void
    {
        try{
            $user = $this->userService->getUserDetails(new UserFilter($userToGroup->getUserId()));
            $group = $this->groupRepository->getGroup($userToGroup->getGroupId());
        } catch (UserServiceException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        } catch (GroupNotFoundStorageException $exception) {
            throw new GroupServiceException(
                GroupServiceException::ERRORS[GroupServiceException::NOT_FOUND_GROUP_ERROR],
                GroupServiceException::NOT_FOUND_GROUP_ERROR,
                $exception);
        } catch (GroupStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        } catch (UsersGroupsStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        try {
            $usersGroups = new UsersGroups(
                $user,
                $group,
                false
            );
            $this->usersGroupsRepository->saveUsersGroups($usersGroups);
        } catch (UsersGroupsStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param int $groupId
     * @return Group
     *
     * @throws GroupServiceException
     */
    public function getGroupDetails(int $groupId): Group
    {
        try {
            $group = $this->groupRepository->getGroup($groupId);

        } catch (GroupNotFoundStorageException $exception) {
            throw new GroupServiceException(
                GroupServiceException::ERRORS[GroupServiceException::NOT_FOUND_GROUP_ERROR],
                GroupServiceException::NOT_FOUND_GROUP_ERROR,
                $exception);
        } catch (GroupStorageException $exception) {
            throw new GroupServiceException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $group;
    }
}
