<?php

namespace VStancescu\Users\Group\Service;


class GroupToCreate
{
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var string */
    private $city;

    /** @var string should be country code RO, NL, TR, UK */
    private $country;

    /** @var integer */
    private $foundedBy;

    /**
     * GroupToCreate constructor.
     * @param string $name
     * @param string $description
     * @param string $city
     * @param string $country
     * @param int $foundedBy
     */
    public function __construct(string $name, string $description, string $city, string $country, int $foundedBy)
    {
        $this->name = $name;
        $this->description = $description;
        $this->city = $city;
        $this->country = $country;
        $this->foundedBy = $foundedBy;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getFoundedBy(): int
    {
        return $this->foundedBy;
    }

}
