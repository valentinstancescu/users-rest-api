<?php

namespace VStancescu\Users\Slim\Authentication;

use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\ErrorApiResponseBuilderInterface;
use VStancescu\Users\Common\Config;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Authentication
{
    const AUTHENTICATION_HEADER_NAME = 'Authorization';
    const AUTHENTICATION_REGEXP_HEADER_VALUE = ' /^ApiKey\s+(.*)$/';
    const AUTHENTICATION_CONFIG_KEY_NAME = 'ApiKey';

    /** @var Config */
    private $config;

    /** @var ErrorApiResponseBuilderInterface */
    private $errorApiResponseBuilder;

    /**
     * @param Config $config
     * @param ErrorApiResponseBuilderInterface $errorApiResponseBuilder
     */
    public function __construct(Config $config, ErrorApiResponseBuilderInterface $errorApiResponseBuilder)
    {
        $this->config = $config;
        $this->errorApiResponseBuilder = $errorApiResponseBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $authenticationHeader = $this->getAuthenticationHeader($request);

        if ($this->isAuthenticated($authenticationHeader)) {
            return $next($request, $response);
        }

        return $this->errorApiResponseBuilder->errorResponse(
            401,
            ApiResponseErrors::AUTHENTICATION_GENERIC_ERROR,
            ApiResponseErrors::ERRORS[ApiResponseErrors::AUTHENTICATION_GENERIC_ERROR]
        );
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string
     */
    private function getAuthenticationHeader(ServerRequestInterface $request)
    {
        $headers = $request->getHeader(self::AUTHENTICATION_HEADER_NAME);
        $header = isset($headers[0]) ? $headers[0] : '';

        return $header;
    }

    /**
     * @param string $authenticationHeader
     *
     * @return bool
     */
    private function isAuthenticated($authenticationHeader)
    {
        if (preg_match(self::AUTHENTICATION_REGEXP_HEADER_VALUE, $authenticationHeader, $matches)) {
            if ($matches[1] === $this->config->get(self::AUTHENTICATION_CONFIG_KEY_NAME)) {
                return true;
            }
        }

        return false;
    }
}
