<?php

namespace VStancescu\Users\Slim;


use DI\Bridge\Slim\App;
use DI\ContainerBuilder;
use VStancescu\Users\DI\ContainerHelper;

class Application extends App
{
    protected function configureContainer(ContainerBuilder $builder)
    {
        ContainerHelper::configureContainerBuilder($builder);
        $builder->addDefinitions(dirname(__DIR__) . '/../config/slim-config.php');
    }
}
