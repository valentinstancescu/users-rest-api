<?php

namespace VStancescu\Users\Slim\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\AbstractHandler;
use VStancescu\Users\Common\Api\ApiResponseErrors;
use VStancescu\Users\Common\Api\ResponseBuilder\ErrorApiResponseBuilderInterface;

class NotFound extends AbstractHandler
{
    /** @var ErrorApiResponseBuilderInterface */
    private $responseBuilder;

    /**
     * NotAllowed constructor.
     * @param ErrorApiResponseBuilderInterface $responseBuilder
     */
    public function __construct(ErrorApiResponseBuilderInterface $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->responseBuilder->errorResponse(
            404,
            ApiResponseErrors::APPLICATION_NOT_FOUND_ERROR,
            ApiResponseErrors::ERRORS[ApiResponseErrors::APPLICATION_NOT_FOUND_ERROR]
        );
    }
}
