<?php

namespace VStancescu\Users\Slim;


use VStancescu\Users\Group\Api\GroupController;
use VStancescu\Users\Slim\Authentication\Authentication;
use VStancescu\Users\User\Api\UserController;

class ApplicationBuilder
{
    /**
     * @return Application
     */
    public function createApplication()
    {
        //define API routes
        $app = new Application();
        $app->group('/api/v1', function () {
            /**@var Application $this */
            $this->group('/group', function () {
                /** @var Application $this */
                $this->post('', [GroupController::class, 'create']);
                $this->post('/{groupId}/user/{userId}', [GroupController::class, 'addUserToGroup']);
                $this->get('/{groupId}', [GroupController::class, 'getGroupDetails']);
            });

            $this->group('/user', function () {
                /** @var Application $this */
                $this->post('', [UserController::class, 'create']);
                $this->post('/{userId}/friend/{friendUserId}', [UserController::class, 'addFriendToUser']);
                $this->get('', [UserController::class, 'getUsers']);
                $this->get('/{userId}', [UserController::class, 'getUserDetails']);
            });

        })->add(Authentication::class);

        return $app;
    }
}
