# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    required_plugins = %w(vagrant-vbguest vagrant-hostmanager)

    plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
    if not plugins_to_install.empty?
        puts "Installing plugins: #{plugins_to_install.join(' ')}"
        if system "vagrant plugin install #{plugins_to_install.join(' ')}"
            exec "vagrant #{ARGV.join(' ')}"
        else
            abort 'Installation of one or more plugins has failed. Aborting.'
        end
    end

    if Vagrant.has_plugin?('vagrant-hostmanager')
        config.hostmanager.enabled = true
        config.hostmanager.manage_host = true
        config.hostmanager.manage_guest = true
        config.hostmanager.ignore_private_ip = false
        config.hostmanager.aliases = 'users.vstancescu.local'
    end

    config.vm.box = 'ubuntu/xenial64'
    config.vm.box_check_update = true
    config.vm.provider :virtualbox do |vb|
        vb.name = 'users'

        vb.memory = 1024
        vb.cpus = 2

        vb.customize ['modifyvm', :id, '--ioapic', 'on']
        vb.customize ['modifyvm', :id, '--cpuexecutioncap', '100']
    end

    config.vm.network :private_network, ip: '192.168.50.10'

    config.vm.synced_folder '.', '/vagrant', disabled: true
    config.vm.synced_folder '.', '/var/www/users', create: true

    config.vm.provision 'docker'

    config.vm.provision 'shell', path: 'dev_env/local/provisioners/provision.sh', name: 'Setup'
    config.vm.provision 'shell', path: 'dev_env/local/provisioners/composer-install.sh', run: 'always', name: 'Composer install'
    config.vm.provision 'shell', path: 'dev_env/local/provisioners/docker.sh', run: 'always', name: 'Docker setup'
end
