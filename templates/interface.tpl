{include file='header.tpl'}


    <div class="container">
        <h1>Welcome to Valentin's implementation</h1>
        <hr/>
    </div>
    <div class="container">

        <table id="usersList" class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">City</th>
                <th scope="col">Country</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            {$cnt = 1}
            {foreach from=$usersCollection item=user}
                <tr>
                    <td>{$cnt}</td>
                    <td>{$user->getFirstName()}</td>
                    <td>{$user->getLastName()}</td>
                    <td><a href="mailto:{$user->getEmail()}">{$user->getEmail()}</a></td>
                    <td>{$user->getCity()}</td>
                    <td>{$user->getCountry()}</td>
                    <td><a href="/user-details.php?id={$user->getId()}">View details</a> </td>
                </tr>

                {$cnt = $cnt + 1}
            {/foreach}
            </tbody>
        </table>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#usersList').DataTable();
            } );
        </script>
    </div>
</body>
</html>
