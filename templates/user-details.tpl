{include file='header.tpl'}
<style type="text/css">
    body {
        background: #F1F3FA;
    }

    /* Profile container */
    .profile {
        margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
        padding: 20px 0 10px 0;
        background: #fff;
    }

    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .profile-usertitle {
        text-align: center;
        margin-top: 20px;
    }

    .profile-usertitle-name {
        color: #5a7391;
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 7px;
    }

    .profile-usertitle-job {
        text-transform: uppercase;
        color: #5b9bd1;
        font-size: 12px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .profile-userbuttons {
        text-align: center;
        margin-top: 10px;
    }

    .profile-userbuttons .btn {
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        padding: 6px 15px;
        margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
        margin-right: 0px;
    }

    .profile-usermenu {
        margin-top: 30px;
    }

    .profile-usermenu ul li {
        border-bottom: 1px solid #f0f4f7;
    }

    .profile-usermenu ul li:last-child {
        border-bottom: none;
    }

    .profile-usermenu ul li a {
        color: #93a3b5;
        font-size: 14px;
        font-weight: 400;
    }

    .profile-usermenu ul li a i {
        margin-right: 8px;
        font-size: 14px;
    }

    .profile-usermenu ul li a:hover {
        background-color: #fafcfd;
        color: #5b9bd1;
    }

    .profile-usermenu ul li.active {
        border-bottom: none;
    }

    .profile-usermenu ul li.active a {
        color: #5b9bd1;
        background-color: #f6f9fb;
        border-left: 2px solid #5b9bd1;
        margin-left: -2px;
    }

    /* Profile Content */
    .profile-content {
        padding: 20px;
        background: #fff;
        min-height: 460px;
    }

</style>


<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">
                    <img src="https://cdn.onlinewebfonts.com/svg/img_87237.png" alt="profile pic" width="200" height="200" />
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {$user->getFirstName()} {$user->getLastName()}
                    </div>
                    <div class="profile-usertitle-job">
                        {$user->getCity()}, {$user->getCountry()}
                    </div>
                    <div class="profile-usertitle-job">
                        Member since: {$user->getDateCreated()->format('d.m.Y')}
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="/user-details.php?id={$user->getId()}">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                        </li>
                        <li>
                            <a href="/user-details.php?id={$user->getId()}&show-friends=true">
                                <i class="glyphicon glyphicon-user"></i>
                                View user friends </a>
                        </li>
                        <li>
                            <a href="/user-details.php?id={$user->getId()}&show-friends=true&country={$user->getCountry()}">
                                <i class="glyphicon glyphicon-flag"></i>
                                View friends from same country</a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">

                <h4>Description:</h4>
                <p>{$user->getDescription()}</p>


                <div class="table-responsives">
                    <h4>Member of following groups:</h4>

                    <table id="groupList" class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Group Name</th>
                            <th scope="col">City</th>
                            <th scope="col">Country</th>
                        </tr>
                        </thead>
                        <tbody>

                        {$cnt = 1}
                        {foreach from=$userGroups item=group}
                            <tr>
                                <td>{$cnt}</td>
                                <td>{$group->getName()}</td>
                                <td>{$group->getCity()}</td>
                                <td>{$group->getCountry()}</td>

                            </tr>
                            {$cnt = $cnt + 1}
                        {/foreach}
                        </tbody>
                    </table>

                </div>

                {if !empty($user->getFriendsWithMe())}
                <div class="table-responsives">
                    <h4>Friends:</h4>
                    <table id="usersList" class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">City</th>
                            <th scope="col">Country</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        {$cnt = 1}
                        {foreach from=$user->getFriendsWithMe() item=user}
                            <tr>
                                <td>{$cnt}</td>
                                <td>{$user->getFirstName()}</td>
                                <td>{$user->getLastName()}</td>
                                <td><a href="mailto:{$user->getEmail()}">{$user->getEmail()}</a></td>
                                <td>{$user->getCity()}</td>
                                <td>{$user->getCountry()}</td>
                                <td><a href="/user-details.php?id={$user->getId()}">View details</a> </td>
                            </tr>

                            {$cnt = $cnt + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>

</body>
</html>
